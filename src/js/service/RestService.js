import {ajax} from 'rxjs/observable/dom/ajax';
import {Observable} from 'rxjs/Observable';

const REST_REQUEST_ERROR = 'REST_REQUEST_ERROR';
export default class RestService {
    static request(path) {
        return ajax.getJSON(path)
            .catch(e => {
                console.error(e);
                return Observable.of({type: REST_REQUEST_ERROR, error: e});
            })
    }
}