import SockJS from 'sockjs-client'
import Stomp from 'stompjs'
import {Observable} from 'rxjs/Observable';
import EventEmitter from 'event-emitter';

const clients = {};
const observables = {};
const subscriptions = {};
export default class WebSocketService {

    static connect(path) {
        if (!clients[path]) {
            clients[path] = Observable.create(observer => {
                const connect = () => {
                    const socket = new SockJS(path);
                    const client = Stomp.over(socket);
                    client.connect({},
                        () => {
                            observer.next(client);
                        },
                        (error) => {
                            console.log(error);
                            setTimeout(connect, 7000);
                            console.log('STOMP: Reconnecting in 7 seconds');
                        }
                    );
                };
                connect();
            }).publishReplay(1)
                .refCount()
                .filter(client => client.ws.readyState === SockJS.OPEN)
        }
        return clients[path];
    }

    static subscribe(path, topic) {
        return observables[path + topic]
            || this.connect(path)
                .flatMap(client => {
                    const eventEmitter = new EventEmitter();
                    subscriptions[path + topic] = client.subscribe(topic, (message) => eventEmitter.emit(topic, message));
                    observables[path + topic] = Observable.fromEvent(
                        eventEmitter,
                        topic,
                        message => JSON.parse(message.body)
                    );
                    return observables[path + topic];
                });
    }

    static unsubscribe(path, topic) {
        if(subscriptions[path+topic]) {
            const subscription = subscriptions[path+topic];
            subscription.unsubscribe();
            delete subscriptions[path+topic];
            delete observables[path+topic];
        }
    }

    static send(path, dest, message) {
        return this.connect(path)
            .first()
            .subscribe(client => client.send(dest, {}, JSON.stringify(message)));
    }
}
