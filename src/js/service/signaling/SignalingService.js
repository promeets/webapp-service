import WebSocketService from '../WebSocketService'
import {SIGNALING_WS_API_PATH, SIGNALING_API_TOPIC_OFFER,
        SIGNALING_API_TOPIC_ANSWER, SIGNALING_API_TOPIC_CANDIDATE, SIGNALING_API_TOPIC_CALL_REQUEST,
        SIGNALING_API_TOPIC_LEAVE,
        SIGNALING_OFFER_BROKER, SIGNALING_ANSWER_BROKER, SIGNALING_CANDIDATE_BROKER,
        SIGNALING_CALL_REQUEST_BROKER, SIGNALING_LEAVE_BROKER } from '../../api/apiPaths'

export default class SignalingService extends WebSocketService {
    static subscribeOnCallRequests(userId) {
        return this.subscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_CALL_REQUEST}/${userId}`)
    }

    static subscribeOnOffers(chatId) {
        return this.subscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_OFFER}/${chatId}`)
    }

    static subscribeOnAnswers(chatId) {
        return this.subscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_ANSWER}/${chatId}`)
    }

    static subscribeOnCandidates(chatId) {
        return this.subscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_CANDIDATE}/${chatId}`)
    }

    static subscribeOnLeaves(chatId) {
        return this.subscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_LEAVE}/${chatId}`)
    }

    static sendCallRequest(from, to, chatId) {
        return this.send(SIGNALING_WS_API_PATH, `${SIGNALING_CALL_REQUEST_BROKER}/${to}`, {from, to, chatId})
    }

    static sendOffer(chatId, from, to, data) {
        return this.send(SIGNALING_WS_API_PATH, `${SIGNALING_OFFER_BROKER}/${chatId}`, {chatId, from, to, type:data.type, data:data.sdp})
    }

    static sendAnswer(chatId, from, to, data) {
        return this.send(SIGNALING_WS_API_PATH, `${SIGNALING_ANSWER_BROKER}/${chatId}`, {chatId, from, to, type: data.type, data:data.sdp})
    }

    static sendCandidate(chatId, from, to, data) {
        return this.send(SIGNALING_WS_API_PATH, `${SIGNALING_CANDIDATE_BROKER}/${chatId}`, {chatId, from, to, type: 'candidate', data: JSON.stringify(data)})
    }

    static sendLeave(chatId, from, to) {
        return this.send(SIGNALING_WS_API_PATH, `${SIGNALING_LEAVE_BROKER}/${chatId}`, {chatId, from, to, type: 'leave', data: ''})
    }

    static unsubscribeFromCall(chatId) {
        this.unsubscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_OFFER}/${chatId}`);
        this.unsubscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_ANSWER}/${chatId}`);
        this.unsubscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_CANDIDATE}/${chatId}`);
        this.unsubscribe(SIGNALING_WS_API_PATH, `${SIGNALING_API_TOPIC_LEAVE}/${chatId}`);
    }
}
