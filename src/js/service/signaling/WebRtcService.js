import {
  Observable
} from 'rxjs/Observable';
import {ajax} from 'rxjs/observable/dom/ajax';
import {sendCandidate} from '../../state/app/call/callAppState';
import {SIGNALING_REST_API_PATH} from '../../api/apiPaths'

const configuration = {
  "iceServers": [{
    "url": "stun:stun2.1.google.com:19302"
  }]
};

let localStream;
let connections = {};

export default class WebRtcService {

    static getActiveParticipantsByChatId = (chatId) =>
        ajax.getJSON(`${SIGNALING_REST_API_PATH}/active/participants/${chatId}`)
            .map(response => {
                let activeParticipants = response;
                return activeParticipants;
            })

    static getUserMedia = (localVideo) =>
      Observable.create(observer => {
        navigator.mediaDevices.getUserMedia({
            audio: false,
            video: true
        }).then(stream => {
            localVideo.src = window.URL.createObjectURL(stream);
            localStream = stream;
            observer.next(stream);
        }).catch(error => observer.error(error));
      });

      static hangUp = (localVideo, remoteVideos) => {
            localStream.getTracks().forEach(track => track.stop());
            localStream = null;
            localVideo.src = null;
            Object.keys(remoteVideos).forEach(participantId => remoteVideos[participantId].src = null);
            Object.keys(connections).forEach(connectionId => {
                let connection = connections[connectionId];
                connection.close();
                connection.onaddstream = null;
                connection.onicecandidate = null;
            })
            connections = {};
      }

      static disconnectFromParticipant = (participantId) => {
            let connection = connections[participantId];
            connection.close();
            connection.onaddstream = null;
            connection.onicecandidate = null;
            delete connections[participantId];
      }

  static createConnections = (dispatch, participantIds, remoteVideos) =>
    Observable.create(observer => {
      try {
          participantIds.map(participantId => {
              let connection = new RTCPeerConnection(configuration);
              console.log("connection created: " + connection);
              connection.addStream(localStream);
              connection.onaddstream = event =>
                remoteVideos[participantId].src = window.URL.createObjectURL(event.stream);
              connection.onicecandidate = event => {
                if (event.candidate)
                  dispatch(sendCandidate(participantId, event.candidate));
              }
              connections[participantId] = connection;
              observer.next({participantId,connection});
          });
      } catch (error) {
        observer.error(error);
      }
    });

    static createConnectionFromOffer = (participantId, offer) =>
        Observable.create(observer => {
            try {
                let connection = new RTCPeerConnection(configuration);
                console.log("connection created: " + connection);
                connection.addStream(localStream);
                connection.setRemoteDescription(new RTCSessionDescription(offer));
                connections[participantId] = connection;
                observer.next({participantId,connection});
            } catch (error) {
                observer.error(error);
            }
        });

    static setOnAddStreamOnCandidateCallback = (participantId, dispatch, remoteVideo) =>
        Observable.create(observer => {
            try {
                let connection = connections[participantId];
                connection.onaddstream = event =>
                  remoteVideo.src = window.URL.createObjectURL(event.stream);
                connection.onicecandidate = event => {
                    if (event.candidate)
                      dispatch(sendCandidate(participantId, event.candidate));
                    }
                observer.next({participantId,connection});
            } catch (error) {
                observer.error(error);
            }
        });

    static addCandidateToConnection = (participantId, candidate) => {
        let connection = connections[participantId];
        return connection ? Observable.fromPromise(connection.addIceCandidate(new RTCIceCandidate(candidate))) :
            Observable.of();
    }

    static createAnswerForParticipant = (participantId) =>
        Observable.create(observer => {
            let connection = connections[participantId];
            connection.createAnswer(answer => {
                connection.setLocalDescription(answer);
                observer.next({participantId,answer});
            }, error => observer.error(error))
        });

    static setAnswerToConnection = (participantId, answer) =>
        Observable.create(observer => {
            try {
                let connection = connections[participantId];
                connection.setRemoteDescription(new RTCSessionDescription(answer));
                console.log("SET ANSWER WAS CALLED!!!");
                observer.next({participantId,answer});
            } catch(error) {observer.error(error);}
        });

    static createOffer = (participantId, connection) =>
      Observable.create(observer => {
        if (connection == null)
          connection = connections[participantId];
        connection.createOffer(offer => {
            connection.setLocalDescription(offer);
            observer.next({
              participantId,
              offer
            });
          },
          error => observer.error(err));
      });
}
