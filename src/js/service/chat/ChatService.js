import {CHAT_API_PATH} from '../../api/apiPaths';
import RestService from '../RestService';

const INLINE_USER_PROJECTION = 'inlineUser';
export default class ChatService extends RestService {

    static fetchChatById(chatId) {
        return this.request(`${CHAT_API_PATH}/chats/${chatId}`)
            .map(response => {
                let {id, name, _links} = response;
                return {id, name, _links};
            })
    }


    static fetchCurrentUser() {
        return this.request(`${CHAT_API_PATH}/users/search/me`)
    }

    static fetchChats(page = '0', orderBy = 'lastUpdate', orders = 'desc', name = '') {
        return this.request(`${CHAT_API_PATH}/chats/search/meByName?page=${page}&sort=${orderBy},${orders}&name=${name}`)
            .map(response => {
                let {_embedded, _links, page} = response;
                page.last = !_links.next;
                return {content: _embedded.chats, meta: page};
            })
    }

    static fetchMessages(chatId, page = '0') {
        return this.request(`${CHAT_API_PATH}/messages/search/byChat?chatId=${chatId}&page=${page}&sort=time,desc`)
            .map(response => {
                let {_embedded, _links, page} = response;
                page.last = !_links.next;
                return {content: _embedded.messages.reverse(), meta: page};
            })
    }


    static fetchParticipants(chatId) {
        return this.request(`${CHAT_API_PATH}/chats/${chatId}/participants`)
            .map(response => {
                let {_embedded} = response;
                return {content: _embedded.participants};
            })
    }

    static fetchParticipantsWithUsers(chatId) {
        return this.request(`${CHAT_API_PATH}/chats/${chatId}/participants?projection=${INLINE_USER_PROJECTION}`)
            .map(response => {
                let {_embedded} = response;
                return {content: _embedded.participants};
            })
    }

}
