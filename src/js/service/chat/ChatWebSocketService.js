import WebSocketService from '../WebSocketService'
import {CHAT_API_WS_PATH, CHAT_MESSAGE_WS_BROKER, CHAT_MESSAGE_WS_TOPIC} from '../../api/apiPaths'
export default class ChatWebSocketService extends WebSocketService {
    static subscribeOnMessages(userId) {
        return this.subscribe(CHAT_API_WS_PATH, CHAT_MESSAGE_WS_TOPIC + "/" + userId)
    }

    static sendMessage(message) {
        return this.send(CHAT_API_WS_PATH, CHAT_MESSAGE_WS_BROKER, message)
    }
}