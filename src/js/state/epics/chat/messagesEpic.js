import {Observable} from 'rxjs/Observable';
import {combineEpics} from 'redux-observable';
import {
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS,
    SEND_MESSAGE
} from '../../app/chat/messagesState';
import {
    SET_CURRENT_USER
} from '../../app/chat/chatUserState';
import {saveChatMessages, saveChatMeta, setChatMessagesMeta, saveChatMessage} from '../../db/chat/chatDbState';
import {currentChatMessagesMeta} from '../../db/chat/chatSelectors';
import ChatWebSocketService from '../../../service/chat/ChatWebSocketService';
import ChatService from '../../../service/chat/ChatService';

const messagesFetchEpic = (action$, store) =>
    action$.ofType(FETCH_MESSAGES_REQUEST)
        .flatMap(() => {
            const chatId = store.getState().app.chat.chatId;
            const meta = currentChatMessagesMeta(store.getState()) || {number: -1};
            return ChatService.fetchMessages(chatId, meta.number + 1)
                .flatMap(response => {
                    const {content, meta} = response;
                    return Observable.concat(
                        Observable.of(saveChatMeta({...meta, id: chatId})),
                        Observable.of(setChatMessagesMeta(chatId, chatId)),
                        Observable.of(saveChatMessages(content, meta, chatId)),
                        Observable.of({type: FETCH_MESSAGES_SUCCESS})
                    )
                })
        });

const receiveMessageEpic = (action$, store) =>
    action$.ofType(SET_CURRENT_USER)
        .flatMap(() => {
            const userId = store.getState().app.chat.user.userId;
            return ChatWebSocketService.subscribeOnMessages(userId)
                .flatMap(message => {
                    return Observable.of(saveChatMessage(message.chatId, message))
                })
        });

const sendMessageEpic = (action$, store) =>
    action$.ofType(SEND_MESSAGE)
        .flatMap(({message}) => {
            ChatWebSocketService.sendMessage(message);
            //ToDo: change for notification about sending result
            return Observable.of();
        });


export const messagesEpic = combineEpics(
    messagesFetchEpic,
    receiveMessageEpic,
    sendMessageEpic
);