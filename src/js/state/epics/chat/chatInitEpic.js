import {Observable} from 'rxjs/Observable';
import {INIT_CHAT, initChatSuccess} from '../../app/chat/chatAppState';
import ChatService from "../../../service/chat/ChatService";
import {setCurrentUser} from '../../app/chat/chatUserState';
import {saveChatUser} from '../../db/chat/chatDbState';

const fetchCurrentUser = () =>
    ChatService.fetchCurrentUser()
        .flatMap(user =>
            Observable.concat(
                Observable.of(saveChatUser(user)),
                Observable.of(setCurrentUser(user.id))
            )
        );

export const initChatEpic = (action$, store) =>
    action$.ofType(INIT_CHAT)
        .flatMap(action =>
            Observable.concat(
                fetchCurrentUser(),
                Observable.of(initChatSuccess())
            )
        );