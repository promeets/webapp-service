import {Observable} from 'rxjs/Observable';
import {combineEpics} from 'redux-observable';
import {
    FETCH_CHATS_REQUEST,
    FETCH_CHATS_SUCCESS,
    FETCH_CHATS_ERROR,
    SET_CHATS_ORDER_BY,
    SET_CHATS_NAME_FILTER
} from '../../app/chat/chatsState';
import {CLOSE_CHAT, OPEN_CHAT} from '../../app/chat/chatAppState';
import {saveChats, deleteChatMeta, saveChatMeta, CHATS_META_ID} from '../../db/chat/chatDbState';
import ChatService from "../../../service/chat/ChatService";
import {chatsMeta} from '../../db/chat/chatSelectors';
import {fetchChats} from '../../../state/app/chat/chatsState';

const chatCloseEpic = (action$, store) =>
    action$.ofType(CLOSE_CHAT)
        .map(_ => deleteChatMeta(store.getState().app.chat.lastChatId));

const chatOpenEpic = (action$, store) =>
    action$.ofType(OPEN_CHAT)
        .map(_ => deleteChatMeta(CHATS_META_ID));

const chatsFetchEpic = (action$, store) =>
    action$.ofType(FETCH_CHATS_REQUEST)
        .flatMap(() => {
                const chats = store.getState().app.chat.chats;
                const meta = chatsMeta(store.getState()) || {number: -1};
                return ChatService.fetchChats(meta.number + 1, chats.orderBy, chats.orders, chats.nameFilter)
                    .flatMap(response => {
                        let {content, meta} = response;
                        return Observable.concat(
                            Observable.of(saveChatMeta({...meta, id: CHATS_META_ID})),
                            Observable.of(saveChats(content)),
                            Observable.of({type: FETCH_CHATS_SUCCESS})
                        );
                    })
            }
        );


const chatsChangeFilterEpic = (action$, store) =>
    action$.ofType(SET_CHATS_NAME_FILTER)
        .debounceTime(400)
        .flatMap(_ => Observable.concat(
            Observable.of(deleteChatMeta(CHATS_META_ID)),
            Observable.of(fetchChats())
        ));

const chatsChangeOrderEpic = (action$, store) =>
    action$.ofType(SET_CHATS_ORDER_BY)
        .flatMap(_ => Observable.concat(
            Observable.of(deleteChatMeta(CHATS_META_ID)),
            Observable.of(fetchChats())
        ));


export const chatsEpic = combineEpics(
    chatsFetchEpic,
    chatCloseEpic,
    chatOpenEpic,
    chatsChangeOrderEpic,
    chatsChangeFilterEpic,
);
