import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {combineEpics} from 'redux-observable';
import {FETCH_PARTICIPANTS_REQUEST, FETCH_PARTICIPANTS_SUCCESS} from '../../app/chat/participantsState';
import ChatService from '../../../service/chat/ChatService';
import {saveChatParticipants, saveChatUsers} from '../../db/chat/chatDbState'

const participantsFetchEpic = (action$, store) =>
    action$.ofType(FETCH_PARTICIPANTS_REQUEST)
        .flatMap(() => {
                const chatId = store.getState().app.chat.chatId;
                return ChatService.fetchParticipantsWithUsers(chatId)
                    .flatMap(response => {
                        const {content} = response;
                        return Observable.concat(
                            Observable.of(saveChatUsers(content.map(v => v.user))),
                            Observable.of(saveChatParticipants(content, chatId)),
                            Observable.of({type: FETCH_PARTICIPANTS_SUCCESS})
                        )
                    })
            }
        );


export const participantsEpic = combineEpics(
    participantsFetchEpic
);