import {combineEpics} from 'redux-observable';
import {messagesEpic} from './messagesEpic'
import {participantsEpic} from './participantsEpic'
import {initChatEpic} from './chatInitEpic';
import {chatsEpic} from './chatsEpic';


export const chatEpic = combineEpics(
    initChatEpic,
    chatsEpic,
    messagesEpic,
    participantsEpic
);
