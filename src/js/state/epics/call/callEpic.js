import {Observable} from 'rxjs/Observable';
import {combineEpics} from 'redux-observable';
import {OPEN_CALL, CREATE_CALL, HANG_UP, FILL_CALL_INFO} from '../../app/call/callAppState';
import {fillCallInfo, redirectToCall, openCall, redirectFromCall} from '../../app/call/callAppState';
import {currentChat as currentChatSelector} from '../../db/chat/chatSelectors';
import {ANSWER_CALL, SEND_CALL_REQUEST, RECEIVE_CALL_REQUEST, receiveCallRequest} from '../../app/call/callRequestAppState';
import SignalingService from '../../../service/signaling/SignalingService';
import ChatService from '../../../service/chat/ChatService';
import WebRtcService from '../../../service/signaling/WebRtcService';
import {INIT_CHAT, openChat} from '../../app/chat/chatAppState';
import {saveChat} from '../../db/chat/chatDbState'
import {chats} from '../../db/chat/chatSelectors';

const openCallEpic = (action$, store) =>
        action$.ofType(OPEN_CALL)
            .flatMap(action => {
                const chatId = store.getState().app.chat.chatId;
                  return Observable.of(fillCallInfo(chatId, action.activeParticipants, false));
                });

const redirectToCallEpic = (action$, store) =>
        action$.ofType(FILL_CALL_INFO)
            .flatMap(action => {
                const chatId = store.getState().app.chat.chatId;
                  redirectToCall(chatId);
                  return Observable.of();
                });

const createCallEpic = (action$, store) =>
        action$.ofType(CREATE_CALL)
            .flatMap(action => {
                const chatId = store.getState().app.chat.chatId;
                  return Observable.of(fillCallInfo(chatId, [], true));
              });

// const hangUpEpic = (action$, store) =>
//         action$.ofType(HANG_UP)
//             .map(action => redirectFromCall());

const subscribeOnCallRequestEpic = (action$, store) =>
  action$.ofType(INIT_CHAT)
  .flatMap(() =>
    SignalingService.subscribeOnCallRequests(store.getState().user.id)
    .flatMap(callRequest => Observable.of(receiveCallRequest(callRequest)))
    .catch(ex => {
      console.log(ex);
      return Observable.of();
    }));

const sendCallRequestEpic = (action$, store) =>
        action$.ofType(SEND_CALL_REQUEST)
            .flatMap(action => {
                SignalingService.sendCallRequest(store.getState().user.id,
                    action.participantId, store.getState().app.call.id);
                return Observable.of();
            });

const answerCallEpic = (action$, store) =>
        action$.ofType(ANSWER_CALL)
            .flatMap(action => {
                const chatIdFromRequest = store.getState().app.callRequest.id;
                if(store.getState().app.chat.chatId === chatIdFromRequest) {
                    return WebRtcService.getActiveParticipantsByChatId(chatIdFromRequest)
                        .map(activeParticipants =>
                            openCall(activeParticipants.filter(participant => participant != store.getState().user.id)));
                }
                return Observable.concat(
                    ChatService.fetchChatById(chatIdFromRequest)
                        .map(chat => saveChat(chat)),
                    Observable.of(openChat(
                        chats(store.getState()).filter(chat => chat.id == chatIdFromRequest)[0]
                    )),
                    WebRtcService.getActiveParticipantsByChatId(chatIdFromRequest)
                            .map(activeParticipants =>
                                openCall(activeParticipants.filter(participant => participant != store.getState().user.id)))
                );
            });

export const callEpic = combineEpics(
    createCallEpic,
    openCallEpic,
    redirectToCallEpic,
    // hangUpEpic,
    subscribeOnCallRequestEpic,
    sendCallRequestEpic,
    answerCallEpic
);
