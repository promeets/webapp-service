import {
  Observable
} from 'rxjs/Observable';
import {
  combineEpics
} from 'redux-observable';
import {INIT_CHAT} from '../../app/chat/chatAppState';
import {SEND_OFFER, RECEIVE_OFFER, SEND_ANSWER, RECEIVE_ANSWER, SEND_CANDIDATE, RECEIVE_CANDIDATE,
        SEND_LEAVE, RECEIVE_LEAVE, OPEN_CALL, CREATE_CALL,
    receiveOffer, receiveAnswer, receiveCandidate, receiveLeave, createConnection} from '../../app/call/callAppState';
import {SEND_CALL_REQUEST, RECEIVE_CALL_REQUEST, receiveCallRequest} from '../../app/call/callRequestAppState';
import SignalingService from '../../../service/signaling/SignalingService'
import WebRtcService from '../../../service/signaling/WebRtcService'

const subscribeOnOfferEpic = (action$, store) =>
  action$.ofType(OPEN_CALL, CREATE_CALL)
  .flatMap(() =>
    SignalingService.subscribeOnOffers(store.getState().app.chat.chatId)
    .map(offer => receiveOffer(offer))
    .catch(ex => {
      console.log(ex);
      return Observable.of();
    }));

const subscribeOnAnswerEpic = (action$, store) =>
  action$.ofType(OPEN_CALL, CREATE_CALL)
  .flatMap(() =>
    SignalingService.subscribeOnAnswers(store.getState().app.chat.chatId)
    .map(answer => receiveAnswer(answer))
    .catch(ex => {
      console.log(ex);
      return Observable.of();
  }));

const subscribeOnCandidateEpic = (action$, store) =>
  action$.ofType(OPEN_CALL, CREATE_CALL)
  .flatMap(() =>
    SignalingService.subscribeOnCandidates(store.getState().app.chat.chatId)
    .map(candidate => receiveCandidate(candidate))
    .catch(ex => {
      console.log(ex);
      return Observable.of();
    }));

const subscribeOnLeaveEpic = (action$, store) =>
  action$.ofType(OPEN_CALL, CREATE_CALL)
  .flatMap(() =>
    SignalingService.subscribeOnLeaves(store.getState().app.chat.chatId)
    .map(leave => receiveLeave(leave))
    .catch(ex => {
      console.log(ex);
      return Observable.of();
    }));


const sendOfferEpic = (action$, store) =>
  action$.ofType(SEND_OFFER)
      .flatMap(action => {
          const state = store.getState();
          SignalingService.sendOffer(state.app.call.id, state.user.id,action.participantId, action.offer);
          return Observable.of();
      });

const sendAnswerEpic = (action$, store) =>
    action$.ofType(SEND_ANSWER)
        .flatMap(action => {
            const state = store.getState();
            SignalingService.sendAnswer(state.app.call.id, state.user.id,action.participantId, action.answer);
            return Observable.of();
        });

const sendCandidateEpic = (action$, store) =>
    action$.ofType(SEND_CANDIDATE)
        .flatMap(action => {
            const state = store.getState();
            SignalingService.sendCandidate(state.app.call.id, state.user.id,action.participantId, action.candidate);
            return Observable.of();
        });

const sendLeaveEpic = (action$, store) =>
    action$.ofType(SEND_LEAVE)
        .flatMap(action => {
            const state = store.getState();
            SignalingService.sendLeave(state.app.call.id, state.user.id,action.participantId);
            return Observable.of();
        });

const receiveOfferEpic = (action$, store) =>
    action$.ofType(RECEIVE_OFFER)
        .filter(action => action.offer.chatId == store.getState().app.call.id)
        .filter(action => action.offer.to == store.getState().user.id)
        .flatMap(action => {
            console.log("offer received");
            let offer = {type: action.offer.type, sdp: action.offer.data};
            return WebRtcService.createConnectionFromOffer(action.offer.from, offer)
                .map(({participantId, connection}) => createConnection(participantId))
                .catch(ex => {
                  console.log(ex);
                  return Observable.of();
                })
        });

const receiveAnswerEpic = (action$, store) =>
    action$.ofType(RECEIVE_ANSWER)
        .filter(action => action.answer.chatId == store.getState().app.call.id)
        .filter(action => action.answer.to == store.getState().user.id)
        .flatMap(action => {
            console.log("answer received");
            let answer = {type: action.answer.type, sdp: action.answer.data};
            return WebRtcService.setAnswerToConnection(action.answer.from, answer)
                .flatMap(({participantId, connection}) => Observable.of())
                .catch(ex => {
                  console.log(ex);
                  return Observable.of();
              });
        });

const receiveCandidateEpic = (action$, store) =>
    action$.ofType(RECEIVE_CANDIDATE)
        .filter(action => action.candidate.chatId == store.getState().app.call.id)
        .filter(action => action.candidate.to == store.getState().user.id)
        .flatMap(action => {
            console.log("candidate received");
            let candidate = JSON.parse(action.candidate.data);
            return WebRtcService.addCandidateToConnection(action.candidate.from, candidate)
                .flatMap(() => Observable.of())
                .catch(ex => {
                  console.log(ex);
                  return Observable.of();
              });
        });



export const signalingEpic = combineEpics(
  subscribeOnOfferEpic,
  subscribeOnAnswerEpic,
  subscribeOnCandidateEpic,
  subscribeOnLeaveEpic,
  sendOfferEpic,
  sendAnswerEpic,
  sendCandidateEpic,
  sendLeaveEpic,
  receiveOfferEpic,
  receiveAnswerEpic,
  receiveCandidateEpic
);
