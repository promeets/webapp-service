export const FETCH_CHATS_REQUEST = 'FETCH_CHATS_REQUEST';
export const FETCH_CHATS_SUCCESS = 'FETCH_CHATS_SUCCESS';
export const FETCH_CHATS_ERROR = 'FETCH_CHATS_ERROR';
export const SET_CHATS_NAME_FILTER = 'SET_CHATS_FILTER';
export const SET_CHATS_ORDER_BY = 'SET_CHATS_ORDER_BY';
export const INIT_CHATS_SEARCH = 'INIT_CHATS_SEARCH';

export const fetchChats = () => ({type: FETCH_CHATS_REQUEST});
export const setChatsNameFilter = (filter) => ({type: SET_CHATS_NAME_FILTER, payload: filter});
export const setChatsOrderBy = (orderBy, orders) => ({
    type: SET_CHATS_ORDER_BY,
    payload: {orderBy: orderBy, orders: orders}
});

const INIT_STATE = {
    nameFilter: undefined,
    orderBy: undefined,
    orders: undefined,
    fetching: false,
};

export const chatsReducer = (chats = INIT_STATE, action) => {
    switch (action.type) {
        case FETCH_CHATS_REQUEST:
            return {...chats, fetching: true};
        case FETCH_CHATS_SUCCESS:
            return {...chats, fetching: false};
        case SET_CHATS_NAME_FILTER:
            return {...chats, nameFilter: action.payload};
        case SET_CHATS_ORDER_BY:
            return {...chats, orderBy: action.payload.orderBy, orders: action.payload.orders};
        default:
            return chats;
    }
};