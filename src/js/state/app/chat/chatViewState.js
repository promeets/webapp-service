export const OPEN_CHAT_VIEW = 'OPEN_CHAT_VIEW';
export const CLOSE_CHAT_VIEW = 'CLOSE_CHAT_VIEW';
export const TOGGLE_CHAT_VIEW = 'TOGGLE_CHAT_VIEW';

export const openChatView = () => ({type: OPEN_CHAT_VIEW});
export const closeChatView = () => ({type: CLOSE_CHAT_VIEW});
export const toggleChatView = () => ({type: TOGGLE_CHAT_VIEW});

export const chatViewReducer = (chat, action) => {
    switch (action.type) {
        case OPEN_CHAT_VIEW:
            return {...chat, opened: true};
        case CLOSE_CHAT_VIEW:
            return {...chat, opened: false};
        case TOGGLE_CHAT_VIEW:
            return {...chat, opened: !chat.opened};
        default:
            return chat;
    }
};