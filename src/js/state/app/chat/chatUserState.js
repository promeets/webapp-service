export const SET_CURRENT_USER = 'SET_CURRENT_USER';

export const setCurrentUser = (userId) => ({type: SET_CURRENT_USER, payload: userId});

const INIT_STATE = {
    userId: undefined
};

export const chatUserReducer = (user = INIT_STATE, action) => {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {...user, userId: action.payload};
        default:
            return user;
    }
};