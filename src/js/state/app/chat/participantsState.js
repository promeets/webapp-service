export const FETCH_PARTICIPANTS_REQUEST = 'FETCH_PARTICIPANTS_REQUEST';
export const FETCH_PARTICIPANTS_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_PARTICIPANTS_REJECT = 'FETCH_MESSAGES_REJECT';

export const fetchParticipants = () => ({type: FETCH_PARTICIPANTS_REQUEST});

export const chatParticipantsReduces = (chat, action) => {
    switch (action.type) {
        case FETCH_PARTICIPANTS_REQUEST:
            return {...chat, fetchingParticipants: true};
        case FETCH_PARTICIPANTS_SUCCESS:
            return {...chat, fetchingParticipants: false};
        default:
            return chat;
    }
};