import {chatViewReducer, OPEN_CHAT_VIEW, CLOSE_CHAT_VIEW, TOGGLE_CHAT_VIEW} from './chatViewState';
import {chatsReducer,} from './chatsState';
import {chatMessagesReduces, RECEIVE_MESSAGE, FETCH_MESSAGES_REQUEST, FETCH_MESSAGES_SUCCESS} from './messagesState';
import {chatParticipantsReduces, FETCH_PARTICIPANTS_REQUEST, FETCH_PARTICIPANTS_SUCCESS} from './participantsState';
import {chatUserReducer} from './chatUserState';
export const OPEN_CHAT = 'OPEN_CHAT';
export const CLOSE_CHAT = 'CLOSE_CHAT';
export const INIT_CHAT = 'INIT_CHAT';
export const INIT_CHAT_SUCCESS = 'INIT_CHAT_SUCCESS';

export const openChat = (chat) => ({type: OPEN_CHAT, payload: chat});
export const closeChat = () => ({type: CLOSE_CHAT});
export const initChat = () => ({type: INIT_CHAT});
export const initChatSuccess = () => ({type: INIT_CHAT_SUCCESS});

const INIT_STATE = {
    chatId: undefined,
    lastChatId: undefined,
    chats: undefined,
    user: undefined,
    opened: true,
    fetchingMessages: false,
    fetchingParticipants: false,
    init: false
};

export const chatAppReducer = (chat = INIT_STATE, action) => {
    switch (action.type) {
        case OPEN_CHAT:
            return {...chat, chatId: action.payload.id};
        case CLOSE_CHAT:
            return {...chat, chatId: undefined, lastChatId: chat.chatId};
        case INIT_CHAT:
            return {...chat, init: false};
        case INIT_CHAT_SUCCESS:
            return {...chat, init: true};
        case OPEN_CHAT_VIEW:
        case CLOSE_CHAT_VIEW:
        case TOGGLE_CHAT_VIEW:
            return chatViewReducer(chat, action);
        case FETCH_MESSAGES_REQUEST:
        case FETCH_MESSAGES_SUCCESS:
        case RECEIVE_MESSAGE:
            return chatMessagesReduces(chat, action);
        case FETCH_PARTICIPANTS_REQUEST:
        case FETCH_PARTICIPANTS_SUCCESS:
            return chatParticipantsReduces(chat, action);
        default:
            return {
                ...chat,
                chats: chatsReducer(chat.chats, action),
                user: chatUserReducer(chat.user, action)
            };
    }
};
