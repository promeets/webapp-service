export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_REJECT = 'FETCH_MESSAGES_REJECT';
export const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const fetchMessages = () => ({type: FETCH_MESSAGES_REQUEST});
export const receiveMessage = (message) => ({type: RECEIVE_MESSAGE, message: message});
export const sendMessage = (message) => ({type: SEND_MESSAGE, message: message});

export const chatMessagesReduces = (chat, {type, message}) => {
    switch (type) {
        case FETCH_MESSAGES_REQUEST:
            return {...chat, fetchingMessages: true};
        case FETCH_MESSAGES_SUCCESS:
            return {...chat, fetchingMessages: false};
        case RECEIVE_MESSAGE:
            return chat;
        default:
            return chat;
    }
};