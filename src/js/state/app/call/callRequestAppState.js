export const SEND_CALL_REQUEST = 'SEND_CALL_REQUEST';
export const RECEIVE_CALL_REQUEST = 'RECEIVE_CALL_REQUEST';
export const CALL_REQUEST_TIMEOUT = 'CALL_REQUEST_TIMEOUT';
export const ANSWER_CALL = 'ANSWER_CALL';

export const sendCallRequest = (participantId) => ({type: SEND_CALL_REQUEST, participantId});
export const receiveCallRequest = (callRequest) => ({type: RECEIVE_CALL_REQUEST, callRequest});
export const callRequestTimeout = () => ({type: CALL_REQUEST_TIMEOUT});
export const answerCall = () => ({type: ANSWER_CALL});

const INIT_STATE = {
    id: undefined,
    from: undefined,
    requiresAnswer: false
};

export const callRequestAppReducer = (callRequest = INIT_STATE, action) => {
    switch (action.type) {
        case RECEIVE_CALL_REQUEST:
            return {...callRequest, id: action.callRequest.chatId,
              requiresAnswer: true, from: action.callRequest.from};
        case CALL_REQUEST_TIMEOUT:
        case ANSWER_CALL:
            return {...callRequest, requiresAnswer: false};
        default:
            return callRequest;
    }
};
