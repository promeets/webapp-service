import {browserHistory} from 'react-router'

export const OPEN_CALL = 'OPEN_CALL';
export const CREATE_CALL = 'CREATE_CALL';
export const HANG_UP = 'HANG_UP';
export const FILL_CALL_INFO = 'FILL_CALL_INFO';
export const SEND_OFFER = 'SEND_OFFER';
export const SEND_ANSWER = 'SEND_ANSWER';
export const SEND_CANDIDATE = 'SEND_CANDIDATE';
export const SEND_LEAVE = 'SEND_LEAVE';
export const RECEIVE_OFFER = 'RECEIVE_OFFER';
export const RECEIVE_ANSWER = 'RECEIVE_ANSWER';
export const RECEIVE_CANDIDATE = 'RECEIVE_CANDIDATE';
export const RECEIVE_LEAVE = 'RECEIVE_LEAVE';
export const CREATE_CONNECTION = 'CREATE_CONNECTION';

export const redirectToCall = (callId) => ({type: 'ROUTING',
    to: browserHistory.push(`/call/${callId}`)});
export const redirectFromCall = () => ({type: 'ROUTING',
    to: browserHistory.push(`/`)});
export const fillCallInfo = (currentChatId, activeParticipants, asCreator) => ({type: FILL_CALL_INFO,
    currentChatId, activeParticipants, asCreator});
export const createCall = () => ({type: CREATE_CALL});
export const openCall = (activeParticipants) => ({type: OPEN_CALL, activeParticipants});
export const hangUp = () => ({type: HANG_UP});

export const sendOffer = (participantId, offer) => ({type: SEND_OFFER,
    participantId, offer});
export const sendAnswer = (participantId, answer) => ({type: SEND_ANSWER,
    participantId, answer});
export const sendCandidate = (participantId, candidate) => ({type: SEND_CANDIDATE,
    participantId, candidate});
export const sendLeave = (participantId) => ({type: SEND_LEAVE,
    participantId});

export const receiveOffer = (offer) => ({type: RECEIVE_OFFER,
    offer});
export const receiveAnswer = (answer) => ({type: RECEIVE_ANSWER,
    answer});
export const receiveCandidate = (candidate) => ({type: RECEIVE_CANDIDATE,
    candidate});
export const receiveLeave = (leave) => ({type: RECEIVE_LEAVE,
    leave});

export const createConnection = (participantId) => ({type: CREATE_CONNECTION,
    participantId});

const INIT_STATE = {
    id: undefined,
    active: false,
    creator: false,
    activeParticipants: []
};

export const callAppReducer = (call = INIT_STATE, action) => {
    switch (action.type) {
        case FILL_CALL_INFO:
            return {...call, id: action.currentChatId, activeParticipants: action.activeParticipants, active: true, creator: action.asCreator};
        case CREATE_CONNECTION:
            let activeParticipants = [...call.activeParticipants, action.participantId];
            return {...call, activeParticipants};
        case HANG_UP:
            return {...call, id: undefined, active: false, creator: false, activeParticipants: []};
        case RECEIVE_LEAVE:
            const reducedParticipantIds = call.activeParticipants.filter(participantId => participantId != action.leave.from);
            return {...call, activeParticipants: reducedParticipantIds};
        default:
            return call;
    }
};
