import {chatAppReducer} from './chat/chatAppState';
import {callAppReducer} from './call/callAppState';
import {callRequestAppReducer} from './call/callRequestAppState'

export const appReducer = (appState = {}, action) => {
    return {
        chat: chatAppReducer(appState.chat, action),
        call: callAppReducer(appState.call, action),
        callRequest: callRequestAppReducer(appState.callRequest, action)
    }
};
