import {ORM, createReducer} from 'redux-orm';
import {chatDbReducer} from './chat/chatDbState';
import {
    Chat,
    ChatMeta,
    ChatUser,
    ChatMessage,
    ChatParticipant
} from './chat/chatModels';

export const db = new ORM();
db.register(
    Chat,
    ChatMeta,
    ChatUser,
    ChatMessage,
    ChatParticipant
);

//It's OK to open one session for all changes according the documentation, see createReducer
export const dbReducer = (dbState = db.getEmptyState(), action) => {
    const session = db.session(dbState);
    try {
        chatDbReducer(session, action)
    }
    catch (ex) {
        console.log(ex);
        return dbState;
    }
    return session.state;
};

