import moment from "moment";
export const CHATS_META_ID = 'CHATS_META';
export const SAVE_CHAT = 'SAVE_CHAT';
export const SAVE_CHATS = 'SAVE_CHATS';
export const SAVE_CHAT_MESSAGES = 'SAVE_CHAT_MESSAGES';
export const SAVE_CHAT_MESSAGE = 'SAVE_CHAT_MESSAGE';
export const SET_CHAT_MESSAGES_META = 'SET_CHAT_MESSAGES_META';
export const SAVE_CHAT_PARTICIPANTS = 'SAVE_CHAT_PARTICIPANTS';
export const DELETE_CHAT_META = 'DELETE_CHAT_META';
export const SAVE_CHAT_META = 'SAVE_CHAT_META';
export const SAVE_CHAT_USERS = 'SAVE_CHAT_USERS';
export const SAVE_CHAT_USER = 'SAVE_CHAT_USER';

export const saveChat = (chat) => ({type: SAVE_CHAT, payload: chat});
export const saveChats = (chats, meta) => ({type: SAVE_CHATS, payload: chats});
export const saveChatMessages = (messages, meta, chatId) => ({
    type: SAVE_CHAT_MESSAGES,
    payload: messages,
    meta: meta,
    chatId: chatId
});
export const saveChatMessage = (chatId, message) => ({type: SAVE_CHAT_MESSAGE, chatId: chatId, payload: message});
export const setChatMessagesMeta = (chatId, metaId) => ({type: SET_CHAT_MESSAGES_META, chatId: chatId, metaId: metaId});
export const saveChatParticipants = (participants, chatId) => ({
    type: SAVE_CHAT_PARTICIPANTS,
    payload: participants,
    id: chatId
});
export const deleteChatMeta = (chatId) => ({type: DELETE_CHAT_META, chatId: chatId});
export const saveChatUsers = (users) => ({type: SAVE_CHAT_USERS, payload: users});
export const saveChatUser = (user) => ({type: SAVE_CHAT_USER, payload: user});
export const saveChatMeta = (meta) => ({type: SAVE_CHAT_META, payload: meta});

//Order is significant
export const chatDbReducer = (session, action) => {
    chatUsersReducer(session, action);
    chatMetaReducer(session, action);
    chatReducer(session, action);
    chatMessagesReducer(session, action);
    chatParticipantsReducer(session, action);
};

const MESSAGE_MERGE_TIME_LIMIT = 60000;
const getLastMessageToMergeWith = (ChatMessage, message) => {
    const lastMessage = ChatMessage.all().last();
    const ref = lastMessage && lastMessage.ref;
    if (ref
        && ref.userId === message.userId
        && Math.abs(moment(ref.time).toDate() - moment(message.time).toDate()) < MESSAGE_MERGE_TIME_LIMIT
    ) {
        return lastMessage;
    }
    return undefined;
};


const chatReducer = ({Chat}, action) => {
    switch (action.type) {
        case SAVE_CHAT:
            Chat.save(action.payload);
            break;
        case SAVE_CHATS:
            action.payload.forEach(payload => {
                const {...chat} = payload;
                Chat.save(chat);
            });
            break;
        case SAVE_CHAT_MESSAGE:
        case SAVE_CHAT_MESSAGES:
            const saveMessage = (message, messages) => {
                if (!messages.toRefArray().some(m => m.id === message.id)) {
                    messages.add(message.id)
                }
            };
            const chat = Chat.withId(action.chatId);
            const {messages} = chat;
            switch (action.type) {
                case SAVE_CHAT_MESSAGE:
                    saveMessage(action.payload, messages);
                    break;
                case SAVE_CHAT_MESSAGES:
                    action.payload.forEach(message => {
                        saveMessage(message, messages);
                    });
                    chat.messagesMeta = action.chatId;
                    break;
            }
            break;
        case SAVE_CHAT_PARTICIPANTS:
            const {participants} = Chat.withId(action.id);
            action.payload.forEach(participant => {
                if (!participants.toRefArray().some(p => p.id === participant.id)) {
                    participants.add(participant.id)
                }
            });
            break;
        case SET_CHAT_MESSAGES_META:
            Chat.withId(action.chatId).messagesMeta = action.metaId;
            break;
    }
};

const chatUsersReducer = ({ChatUser}, action) => {
    switch (action.type) {
        case SAVE_CHAT_USER:
            ChatUser.save(action.payload);
            break;
        case SAVE_CHAT_USERS:
            action.payload.forEach(user =>
                ChatUser.save(user)
            );
            break;
    }
};

const chatMetaReducer = ({ChatMeta}, action) => {
    switch (action.type) {
        case SAVE_CHAT_META:
            ChatMeta.save(action.payload);
            break;
        case DELETE_CHAT_META:
            ChatMeta.withId(action.chatId).delete();
            break;
    }
};


const chatMessagesReducer = ({ChatMessage}, action) => {
    const saveMessage = (message) => {
        if(ChatMessage.hasId(message.id))return;
        const lastMessage = getLastMessageToMergeWith(ChatMessage, message);
        ChatMessage.save({
            ...message,
            hideHeader: !!lastMessage,
            user: message.userId,
            chat: message.chatId,
            action: message.action && JSON.parse(message.action)
        });

    };
    switch (action.type) {
        case SAVE_CHAT_MESSAGES:
            action.payload.forEach(message => {
                saveMessage(message)
            });
            break;
        case SAVE_CHAT_MESSAGE:
            saveMessage(action.payload);
            break;
    }
};


const chatParticipantsReducer = ({ChatParticipant}, action) => {
    switch (action.type) {
        case SAVE_CHAT_PARTICIPANTS:
            action.payload.forEach(participant =>
                ChatParticipant.save({
                    id: participant.id,
                    user: participant.userId,
                    chat: participant.chatId
                })
            );
            break;
    }
};
