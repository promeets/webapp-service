import {attr, many, fk, oneToOne} from 'redux-orm';
import {SavedModel} from "../SavedModel";

export class ChatUser extends SavedModel {
    static get modelName() {
        return 'ChatUser';
    }
}

export class ChatParticipant extends SavedModel {
    static get fields() {
        return {
            user: fk('ChatUser'),
            chat: fk('Chat'),
        };
    }

    static get modelName() {
        return 'ChatParticipant';
    }
}

export class Chat extends SavedModel {
    static get fields() {
        return {
            participants: many('ChatParticipant'),
            messages: many('ChatMessage'),
            messagesMeta: oneToOne('ChatMeta')
        };
    }

    static get modelName() {
        return 'Chat';
    }
}

export class ChatMessage extends SavedModel {
    static get modelName() {
        return 'ChatMessage';
    }

    static get fields() {
        return {
            user: fk('ChatUser'),
            text: attr()
        };
    }
}

export class ChatMeta extends SavedModel {
    static get modelName() {
        return 'ChatMeta';
    }
}
