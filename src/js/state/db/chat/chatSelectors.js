import {createSelector} from 'reselect';
import {createSelector as createOrmSelector} from 'redux-orm';
import {db} from '../db';
import {CHATS_META_ID} from './chatDbState';

const ormSelector = state => state.db;

export const chats = createSelector(
    ormSelector,
    state => state.app.chat.chats.nameFilter || (''),
    state => state.app.chat.chats.orderBy || 'lastUpdate',
    state => state.app.chat.chats.orders || 'asc',
    createOrmSelector(db, ({Chat}, nameFilter, orderBy, orders) => {
        return Chat.all()
            .orderBy(orderBy, orders)
            .filter(chat => chat.name.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
            .toRefArray();
    })
);

export const chatsMeta = createSelector(
    ormSelector,
    createOrmSelector(db, ({ChatMeta}) => {
        return ChatMeta.hasId(0) && ChatMeta.withId(CHATS_META_ID).ref;
    })
);

export const currentChat = createSelector(
    ormSelector,
    state => state.app.chat.chatId,
    createOrmSelector(db, ({Chat}, id) => {
        const chat = Chat.withId(id);
        return {
            ...chat.ref,
            messages: chat.messages.toRefArray(),
            participants: chat.participants.toModelArray().map(p => {
                const ref = p.ref;
                return {
                    user: p.user.ref,
                    userId: ref.user,
                    chatId: ref.chat,
                    id: ref.id
                }
            })
        };
    })
);

export const currentChatMessages = createSelector(
    ormSelector,
    state => state.app.chat.chatId,
    createOrmSelector(db, ({Chat}, id) => {
        const {messages} = Chat.withId(id);
        return messages && messages
                .orderBy('time', 'desc')
                .toModelArray().map(m => {
                    return {...m.ref, user: m.user.ref}
                })
    })
);

export const currentChatMessagesMeta = createSelector(
    ormSelector,
    state => state.app.chat.chatId,
    createOrmSelector(db, ({Chat}, id) => {
        const {messagesMeta} = Chat.withId(id);
        return messagesMeta && messagesMeta.ref;
    })
);

export const currentChatParticipants = createSelector(
    ormSelector,
    state => state.app.chat.chatId,
    createOrmSelector(db, ({Chat}, id) => {
        const chat = Chat.withId(id);
        return chat.participants.toModelArray().map(p => {
            const ref = p.ref;
            return {
                user: p.user.ref,
                userId: ref.user,
                chatId: ref.chat,
                id: ref.id
            }
        });
    })
);

export const currentUser = createSelector(
    ormSelector,
    state => state.app.chat.user.userId,
    createOrmSelector(db, ({ChatUser}, id) => {
        return ChatUser.hasId(id) && ChatUser.withId(id).ref;
    })
);