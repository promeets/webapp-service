import {combineEpics} from 'redux-observable';
import {chatEpic} from './epics/chat/chatEpic';
import {callEpic} from './epics/call/callEpic';
import {signalingEpic} from './epics/webrtc/signalingEpic';

export const rootEpic = combineEpics(
    chatEpic,
    callEpic,
    signalingEpic
);