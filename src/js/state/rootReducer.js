import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux';
import userReducer from '../component/common/auth/userReducer';
import {dbReducer} from './db/db';
import {appReducer} from './app/app';
import headerReducer from "../component/home/header/headerReducer";
import projectListReducer from "../component/home/projectList/projectListReducer";
import projectReducer from "../component/home/projectList/project/projectReducer";
import taskReducer from "../component/home/projectList/project/taskList/task/taskReducer";
const rootReducer = combineReducers({
    app: appReducer,
    db: dbReducer,
    user: userReducer,
    routing: routerReducer,
    header: headerReducer,
    projects: projectListReducer,
    project: projectReducer,
    task: taskReducer
});


export default rootReducer
