export const RESOURCE_API_PATH = "/api/resource";
export const AUTH_API_PATH = "/api/auth";

export const CHAT_API_PATH = "/api/chat";
export const CHAT_API_WS_PATH = "/api/chat/ws";
export const CHAT_MESSAGE_WS_TOPIC = '/topic/chat/message';
export const CHAT_MESSAGE_WS_BROKER = '/app/chat/message';


//SIGNALING
export const SIGNALING_REST_API_PATH = "/api/signaling/rest";
export const SIGNALING_WS_API_PATH = "/api/signaling/ws";
export const SIGNALING_API_TOPIC_CALL_REQUEST = "/topic/callrequest";
export const SIGNALING_API_TOPIC_OFFER = "/topic/offer";
export const SIGNALING_API_TOPIC_ANSWER = "/topic/answer";
export const SIGNALING_API_TOPIC_CANDIDATE = "/topic/candidate";
export const SIGNALING_API_TOPIC_LEAVE = "/topic/leave";
export const SIGNALING_CALL_REQUEST_BROKER = "/app/callrequest";
export const SIGNALING_OFFER_BROKER = "/app/offer";
export const SIGNALING_ANSWER_BROKER = "/app/answer";
export const SIGNALING_CANDIDATE_BROKER = "/app/candidate";
export const SIGNALING_LEAVE_BROKER = "/app/leave";
