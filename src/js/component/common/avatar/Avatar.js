import React from 'react';
import {
    Avatar
} from 'material-ui';
class Avatar extends React.Component {
    render() {
        return (
            <Avatar
                style={this.props.style}
                backgroundColor={this.props.style.color}
            >
                {this.props.name[0]}
            </Avatar>
        )
    }
}
Avatar.propTypes = {
    imageUrl: React.PropTypes.string,
    name: React.PropTypes.string,
    color: React.PropTypes.string
};
export default Avatar;