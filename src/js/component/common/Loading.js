import React from 'react';
import {CircularProgress} from 'material-ui';
import {primary} from '../../styles/colors';
import PropTypes from 'prop-types';

const styles = {
    box: {
        position: 'absolute',
        margin: 'auto',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        width: 100,
        height: 100
    },
    progress: {
        display: 'block',
        margin: 'auto',
        textAlign: 'center'
    },
    text: {
        color: primary,
        fontFamily: 'Roboto Light'
    }
};
export default class Loading extends React.PureComponent {
    render() {
        return (
            <div style={styles.box}>
                <h2 style={styles.text}>{this.props.title}</h2>
                <CircularProgress size={30} thickness={2} style={styles.progress}/>
            </div>
        )
    }
}

Loading.propTypes = {
    title: PropTypes.string
};
