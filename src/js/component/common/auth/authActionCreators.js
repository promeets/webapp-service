import Rest from 'rest';
import {browserHistory} from 'react-router'
import {RESOURCE_API_PATH} from '../../../api/apiPaths'

export const AUTH_REQUEST = "AUTH_REQUEST";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_ERROR = "AUTH_ERROR";

function createAuthSuccessAction(json) {
    const entity = JSON.parse(json.entity);
    return {
        type: AUTH_SUCCESS,
        id: entity.userId,
        email: entity.email,
        firstName: entity.firstName,
        lastName: entity.lastName,
        links: entity._links,
        authenticated: true
    }
}

function createAuthErrorAction() {
    return {
        type: AUTH_ERROR,
        error: "You need to log in."
    }
}

export function createAuthAction() {
    return (dispatch) => {
        dispatch({type: 'AUTH_REQUEST'});
        return Rest({
            path: RESOURCE_API_PATH + '/users/search/me'
        }).then((response) => {
                if (response.status.code === 200) {
                    dispatch(createAuthSuccessAction(response));
                } else {
                    dispatch(createAuthErrorAction());
                    dispatch({type: "ROUTING", to: browserHistory.push("/login")});
                }
            },
            (error) => {
                dispatch(createAuthErrorAction());
                dispatch({type: "ROUTING", to: browserHistory.push("/login")});
            }
        );
    }
}
