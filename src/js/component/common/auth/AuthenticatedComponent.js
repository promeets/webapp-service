import React from 'react';
import {connect} from 'react-redux';
import {createAuthAction} from './authActionCreators'
import  Loading from '../Loading';
export function requireAuthentication(Component) {

    class AuthenticatedComponent extends React.Component {

        componentWillMount() {
            this.checkAuth();
        }

        checkAuth() {
            if (!this.props.user.authenticated) {
                this.props.dispatch(createAuthAction());
            }
        }

        render() {
            let innerContent = null;
            if (this.props.user.authenticated) {
                innerContent = <Component {...this.props}/>;
            } else if (!this.props.user.authenticated && this.props.user.isFetching) {
                innerContent = <Loading title={"Promeets"}/>;
            }
            return (
                <div>
                    {innerContent}
                </div>
            );
        }
    }

    const mapStateToProps = (state) => ({
        user: state.user
    });

    return connect(mapStateToProps)(AuthenticatedComponent);

}
