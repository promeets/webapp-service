import {AUTH_REQUEST, AUTH_SUCCESS, AUTH_ERROR} from "./authActionCreators"
import {LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_ERROR} from "../../login/loginActionCreators"
import {LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_ERROR} from "../../home/header/headerActionCreators"

const INITIAL_STATE = {isFetching: false, authenticated: false};

function userReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case AUTH_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                id: action.id,
                email: action.email,
                firstName: action.firstName,
                lastName: action.lastName,
                links: action.links,
                authenticated: action.authenticated
            });
        case LOGIN_ERROR:
        case AUTH_ERROR:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error
            });
        case LOGIN_REQUEST:
        case AUTH_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case LOGOUT_SUCCESS:
            return INITIAL_STATE;
        default:
            return state
    }
}

export default userReducer
