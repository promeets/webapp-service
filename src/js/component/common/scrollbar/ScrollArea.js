import React from 'react';
import {Scrollbars} from 'react-custom-scrollbars';
export default class ScrollArea extends React.Component {
    constructor(props) {
        super(props);
        this.state = {fitted: false};
    }

    componentDidUpdate() {
        if (this.props.anchorToBottom && !this.state.fitted) {
            this.refs.scrollbars.scrollToBottom();
            this.setState({...this.state, fitted: true});
        }
    }

    render() {
        let renderProps = {};
        if (this.props.hideScrollbar) {
            renderProps.renderThumbHorizontal = () => <div/>;
            renderProps.renderThumbVertical = () => <div/>;
        }
        return (
            <Scrollbars
                ref='scrollbars'
                style={{height: 'auto', ...this.props.style}}
                {...renderProps}
            >
                {this.props.children}
            </Scrollbars>
        );
    }
}

ScrollArea.propTypes = {
    hideScrollbar: React.PropTypes.bool,
    anchorToBottom: React.PropTypes.bool,
};
