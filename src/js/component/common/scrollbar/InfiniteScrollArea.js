import React from 'react';
import {Scrollbars} from 'react-custom-scrollbars';
import {CircularProgress} from "material-ui";

const SCROLL_TOP_LOADING_THRESHOLD = 10;
const style = {
    spinner: {
        display: 'block',
        textAlign: 'center',
        margin: 10,
        width: 'auto'
    }
};
export default class InfiniteScrollArea extends React.PureComponent {
    static get defaultProps() {
        return {
            content: [],
            fetching: false,
            onScrollToEnd: () => {
            }
        }
    };

    constructor(props) {
        super(props);
        if (this.props.anchorToBottom) {
            this.props.content.reverse();
        }
        this.state = {mounted: true, scrollTop: 0, firstLoad: false, loaded: false}
    }

    componentDidMount() {
        if (!this.state.loaded) {
            //this.props.onScrollToEnd();
            //this.setState({...this.state, loaded: true});
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.reversed) {
            nextProps.content.reverse();
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.anchorToBottom && this.state.mounted) {
            this.refs.scrollbars.scrollToBottom();
        }
        if (!this.state.mounted && this.props.content.length > prevProps.content.length) {
            this.refs.scrollbars.scrollTop(this.refs.scrollbars.getScrollHeight() - this.state.scrollTop);
        }
    }

    handleScroll = (event) => {
        const list = event.target;
        if (list.scrollTop + list.offsetHeight > list.scrollHeight) {
            this.setState({...this.state, mounted: true});
            if (!this.props.anchorToBottom && !this.props.fetching) {
                this.props.onScrollToEnd();
                this.setState({...this.state, scrollTop: list.scrollTop, firstLoad: true})
            }
        }
        else {
            this.setState({...this.state, mounted: false})
        }
        if (this.props.anchorToBottom && list.scrollTop < SCROLL_TOP_LOADING_THRESHOLD && !this.props.fetching) {
            this.props.onScrollToEnd();
            this.setState({...this.state, scrollTop: list.scrollHeight, firstLoad: true})
        }
    };


    render() {
        let renderProps = {};
        if (this.props.hideScrollbar) {
            renderProps.renderThumbHorizontal = () => <div/>;
            renderProps.renderThumbVertical = () => <div/>;
        }
        //ToDo: magic number 4
        let spinner = this.props.fetching && this.props.content.length < 10 &&
            <CircularProgress
                size={20}
                thickness={2}
                style={style.spinner}
            />;
        return (
            <Scrollbars
                ref='scrollbars'
                onScroll={this.handleScroll}
                style={{height: 'auto', ...this.props.style}}
                {...renderProps}
            >
                <div style={{paddingBottom: 20}}>
                    {this.props.anchorToBottom && spinner}
                    {this.props.content}
                    {!this.props.anchorToBottom && spinner}
                </div>
            </Scrollbars>
        );
    }
}

InfiniteScrollArea.propTypes = {
    content: React.PropTypes.array,
    hideScrollbar: React.PropTypes.bool,
    anchorToBottom: React.PropTypes.bool,
    onScrollToEnd: React.PropTypes.func,
    fetching: React.PropTypes.bool,
    reversed: React.PropTypes.bool
};

