import {TOGGLE_DRAWER, TOGGLE_CHAT} from './headerActionCreators'

const INITIAL_STATE = {isDrawerOpen: false, isChatOpen: true};

function headerReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TOGGLE_DRAWER:
            return Object.assign({}, state, {
                isDrawerOpen: !state.isDrawerOpen
            });
        case TOGGLE_CHAT:
            return Object.assign({}, state, {
                isChatOpen: !state.isChatOpen
            });
        default:
            return state
    }
}

export default headerReducer