import React from 'react';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Avatar from 'material-ui/Avatar';
import ListItem from 'material-ui/List/ListItem';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import IconMenu from 'material-ui/IconMenu';
import FlatButton from 'material-ui/FlatButton';

const styles = {
    icon: {
        color: '#FFFFFF',
        fontStyle: 'normal',
        paddingRight: '0px'
    },
    avatar: {
        margin: 5
    },
    listItem: {
        color: '#FFFFFF'
    },
    search: {
        height: '30px',
        width: '200px',
        background: '#fff'
    },
    searchHint: {
        bottom: '0px',
        height: '100%'
    },
    searchIcon: {
        marginTop: '4px'
    },
    searchHintText: {
        display: 'inline-block',
        verticalAlign: 'super'
    },
    profileButton: {
        height: '100%'
    }
}

class Header extends React.Component {
    render() {
        return (
            <Toolbar style={{background: this.props.muiTheme.palette.primary1Color,
                boxShadow: '0 2px 5px rgba(0,0,0,.26)', height: '46px'}}>
                <ToolbarGroup firstChild={true}>
                    <IconButton
                        iconClassName="material-icons"
                        tooltip="Menu"
                        onClick={this.props.onMenuButtonClick}
                    >
                        <i style={styles.icon}>menu</i>
                    </IconButton>
                </ToolbarGroup>
                <ToolbarGroup lastChild={true}>
                    <TextField
                        hintText={<div><i className="material-icons"
                                          style={styles.searchIcon}>search</i><div style={styles.searchHintText}>Search</div></div>}
                        style={styles.search}
                        hintStyle={styles.searchHint}
                        underlineShow={false}
                    />
                    <IconMenu
                        iconButtonElement={
                            <FlatButton
                                icon={
                                    <Avatar
                                        src="http://www.unmotivating.com/wp-content/uploads/2015/03/health-hack-.12jpg-320x320.jpg"
                                        size={26}
                                        style={styles.avatar}
                                    />
                                }
                                label="▾"
                                labelPosition='after'
                                labelStyle={styles.icon}
                                style={styles.profileButton}
                            >


                            </FlatButton>
                        }
                        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                        targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    >
                        <MenuItem value="1" primaryText="Profile" />
                        <MenuItem value="2" primaryText="Sign out" onClick={this.props.onLogOutClick} />
                    </IconMenu>
                    <IconButton
                        iconClassName="material-icons"
                        tooltip="Notifications"
                    >
                        <i style={styles.icon}>notifications</i>
                    </IconButton>
                </ToolbarGroup>
            </Toolbar>
        )
    }
}

export default muiThemeable()(Header)
