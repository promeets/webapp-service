import React from 'react';
import {TextField, IconButton, ListItem, Avatar} from 'material-ui';
import {NavigationClose, ActionSearch} from 'material-ui/svg-icons';
import {connect} from 'react-redux';
import {currentUser} from "../../../../state/db/chat/chatSelectors";
import {color} from '../../../../styles/colors';
import {setChatsNameFilter} from '../../../../state/app/chat/chatsState';
const headerStyles = {
    searchTextField: {
        marginLeft: '10px',
        marginRight: '10px',
        height: 46
    }
};
class ChatListHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isSearch: props.filter, searchValue: props.filter};
    };

    handleSearchChange = (event) => {
        const value = event.target.value;
        this.setState({...this.state, searchValue: value});
        this.props.setNameFilter(value);
    };

    triggerSearch = () => {
        const isSearch = this.state.isSearch;
        this.setState({
            ...this.state,
            isSearch: !isSearch,
            searchValue: ''
        });
        if (isSearch) {
            this.props.setNameFilter('');
        }
    };

    render() {
        return (
            <div>
                {!this.state.isSearch ?
                    <ListItem
                        style={{paddingTop: 16, paddingBottom: 14, fontSize: 14}}
                        disabled={true}
                        leftAvatar={
                            <Avatar
                                size={30}
                                backgroundColor={color(this.props.user.firstName[0])}
                                style={{top: 8, right: 4}}
                            >
                                {this.props.user.firstName[0]+this.props.user.lastName[0]}
                            </Avatar>
                        }
                        rightAvatar={
                            <IconButton style={{top: 0, right: 4}}
                                        onClick={this.triggerSearch}>
                                <ActionSearch size={30}/>
                            </IconButton>
                        }
                        primaryText={this.props.user.fullName}
                    />
                    :
                    <div>
                        <TextField
                            autoFocus
                            hintText="Search"
                            ref={(input) => {
                                this.nameInput = input;
                            }}
                            style={headerStyles.searchTextField}
                            value={this.state.searchValue}
                            onChange={this.handleSearchChange}
                        />
                        <IconButton style={{top: 0, right: 4, position: 'absolute'}}
                                    onClick={this.triggerSearch}>
                            <NavigationClose size={30}/>
                        </IconButton>
                    </div>
                }
            </div>
        );
    }
}

export default connect(
    (state) => {
        return {
            filter: state.app.chat.chats.nameFilter,
            user: currentUser(state) || {firstName: []}
        }
    },
    (dispatch) => {
        return {
            setNameFilter: (filter) => {
                dispatch(setChatsNameFilter(filter));
            }
        }
    }
)(ChatListHeader)
