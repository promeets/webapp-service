import React from 'react';
import {
    ListItem,
    Avatar
} from 'material-ui';
import {color} from '../../../../styles/colors';
class ChatListItem extends React.Component {

    render() {
        return <div>
            <ListItem
                leftAvatar={
                    <Avatar
                        size={40}
                        backgroundColor={color(this.props.chat.name[0])}
                        onClick={() => this.setState({open: true})}
                    >
                        {this.props.chat.name[0]}
                    </Avatar>
                }
                primaryText={this.props.chat.name}
                onClick={this.props.onItemClick}
            />
        </div>

    };
}
ChatListItem.propTypes = {
    chat: React.PropTypes.object,
    onItemClick: React.PropTypes.func
};
export default ChatListItem;