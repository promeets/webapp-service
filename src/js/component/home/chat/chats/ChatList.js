import React from 'react';
import {
    Paper,
    Divider,
    List,
    Subheader,
    CircularProgress
} from 'material-ui';
import ScrollArea from '../../../common/scrollbar/ScrollArea';
import InfiniteScrollArea from '../../../common/scrollbar/InfiniteScrollArea';
import ChatListToolbar from './ChatListToolbar';
import ChatListItem from './ChatListItem';
import ChatListHeader from './ChatListHeader';
import {connect} from 'react-redux';
import {fetchChats} from '../../../../state/app/chat/chatsState';
import {openChat} from '../../../../state/app/chat/chatAppState';
import {chats, chatsMeta} from '../../../../state/db/chat/chatSelectors';

const contactsListStyles = {
    scrollArea: {
        position: 'absolute',
        top: 98,
        bottom: 0,
        left: 0,
        right: 0
    },
    spinner: {
        width: '100%',
        textAlign: 'center'
    }
};
class ChatList extends React.Component {

    componentDidMount() {
        this.props.fetchChats();
    }

    fetch = () => {
        if (!this.props.chatsMeta.last) {
            this.props.fetchChats();
        }
    };

    render() {
        return (
            <Paper zDepth={1}
                   style={{height: '100%', overflow: 'hidden'}}
            >
                <ChatListHeader/>
                <Divider/>
                <ChatListToolbar/>
                <Divider/>
                <List>
                    <InfiniteScrollArea
                        content={[
                            <Subheader key={0}>My chats</Subheader>,
                            ...this.props.chats.map((chat, i) =>
                                <ChatListItem
                                    key={i + 1}
                                    chat={chat}
                                    onItemClick={() => this.props.openChat(chat)}
                                />
                            )
                        ]}
                        style={contactsListStyles.scrollArea}
                        fetching={this.props.fetching}
                        onScrollToEnd={this.fetch}
                    >
                    </InfiniteScrollArea>
                </List>
            </Paper>
        )
    }
}


export default connect(
    (state) => {
        return {
            chats: chats(state),
            fetching: state.app.chat.chats.fetching,
            chatsMeta: chatsMeta(state)
        }
    },
    (dispatch) => {
        return {
            fetchChats: () => {
                dispatch(fetchChats())
            },
            openChat: (chat) => {
                dispatch(openChat(chat))
            }
        }
    }
)(ChatList)

