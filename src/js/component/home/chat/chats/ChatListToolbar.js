import React from 'react';
//material-ui
import {IconButton, Checkbox} from 'material-ui'
import {
    AvSortByAlpha,
    ContentAdd
} from 'material-ui/svg-icons';
import {connect} from 'react-redux';
import {setChatsOrderBy} from '../../../../state/app/chat/chatsState';
import {toolbarCheckboxStyle} from '../../../../styles/common';
class ChatListToolbar extends React.Component {

    setOrderBy = () => {
        this.props.setOrderBy(this.props.orderBy ? undefined : 'name', 'asc')
    };

    render() {
        return (
            <div style={{textAlign: 'left'}}>
                <Checkbox
                    style={toolbarCheckboxStyle}
                    checkedIcon={<AvSortByAlpha/>}
                    uncheckedIcon={<AvSortByAlpha/>}
                    onCheck={this.setOrderBy}
                    checked={this.props.orderBy}
                />
                <IconButton>
                    <ContentAdd/>
                </IconButton>
            </div>
        )
    }
}
export default connect(
    (state) => {
        return {
            orderBy: state.app.chat.chats.orderBy
        }
    },
    (dispatch) => {
        return {
            setOrderBy: (orderBy, orders) => {
                dispatch(setChatsOrderBy(orderBy, orders))
            }
        }
    }
)(ChatListToolbar)