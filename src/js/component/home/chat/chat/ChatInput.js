import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {TextField, IconButton, Divider} from 'material-ui';
import {ContentSend} from 'material-ui/svg-icons'
import {connect} from 'react-redux';
import {sendMessage} from '../../../../state/app/chat/messagesState';
import {currentChat} from '../../../../state/db/chat/chatSelectors';
class ChatInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
    }

    handleChange = (event) => {
        this.setState({
            value: event.target.value,
        });
    };

    handleKeyDown = (event) => {
        if (event.keyCode === 13 && !event.shiftKey) {
            event.preventDefault();
            this.sendMessage();
        }
    };

    sendMessage = () => {
        if(!this.state.value) return;
        const text = this.state.value;
        this.setState({...this.state, value: ""});
        this.props.sendMessage({
            text: text,
            time: new Date(),
            chatId: this.props.chat.id
        });
    };

    render() {
        return (
            <div>
                <TextField
                    style={{fontSize: 13, marginLeft: 10, marginRight: 10}}
                    hintText="Enter message here"
                    multiLine={true}
                    rows={1}
                    rowsMax={4}
                    value={this.state.value}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                />
                <IconButton
                    style={{bottom: 30, right: 4, position: 'absolute'}}
                    iconStyle={{color: this.props.muiTheme.palette.primary1Color}}
                    onClick={this.sendMessage}
                >
                    <ContentSend/>
                </IconButton>
                <Divider/>
                <div style={{height: 28, width: '100%'}}>

                </div>
            </div>
        );
    }
}

export default connect(
    (state) => {
        return {
            chat: currentChat(state)
        }
    },
    (dispatch) => {
        return {
            sendMessage: message => dispatch(sendMessage(message))
        }
    }
)(muiThemeable()(ChatInput));