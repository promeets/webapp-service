import React from 'react';
import {TextField, IconButton, ListItem, Avatar} from 'material-ui';
import {NavigationClose, ActionSearch} from 'material-ui/svg-icons';
import {connect} from 'react-redux';
import {currentChat, currentChatParticipants} from "../../../../state/db/chat/chatSelectors";
import {fetchParticipants} from '../../../../state/app/chat/participantsState';
import {color} from '../../../../styles/colors';
const headerStyles = {
    searchTextField: {
        marginLeft: '10px',
        marginRight: '10px',
        height: 46
    }
};
class ChatHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isSearch: false};
    }

    componentDidMount() {
        this.props.fetchParticipants();
    }

    render() {
        return (
            <div>
                {!this.state.isSearch ?
                    <ListItem
                        style={{paddingTop: 16, paddingBottom: 14, fontSize: 14}}
                        disabled={true}
                        leftAvatar={
                            <Avatar
                                size={30}
                                backgroundColor={color(this.props.chat.name[0])}
                                style={{top: 8, right: 4}}
                            >
                                {this.props.chat.name[0]}
                            </Avatar>
                        }
                        rightAvatar={
                            <IconButton style={{top: 0, right: 4}}
                                        onClick={() => this.setState({...this.state, isSearch: true})}>
                                <ActionSearch size={30}/>
                            </IconButton>
                        }
                        primaryText={this.props.chat.name}
                    />
                    :
                    <div>
                        <TextField
                            hintText="Search"
                            style={headerStyles.searchTextField}
                        />
                        <IconButton style={{top: 0, right: 4, position: 'absolute'}}
                                    onClick={() => this.setState({...this.state, isSearch: false})}>
                            <NavigationClose size={30}/>
                        </IconButton>
                    </div>
                }
            </div>
        );
    }
}

export default connect(
    (state) => {
        return {
            chat: currentChat(state),
            participants: currentChatParticipants(state)
        }
    },
    (dispatch) => {
        return {
            fetchParticipants: () => dispatch(fetchParticipants())
        }
    }
)(ChatHeader)
