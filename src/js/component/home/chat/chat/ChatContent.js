import React from 'react';
import {Avatar, CircularProgress} from 'material-ui';
import InfiniteScrollArea from '../../../common/scrollbar/InfiniteScrollArea';
import Message from '../message/Message';
import {connect} from 'react-redux';
import {fetchMessages} from '../../../../state/app/chat/messagesState';
import {
    currentChat,
    currentChatMessagesMeta,
    currentChatMessages,
    currentUser
} from '../../../../state/db/chat/chatSelectors';
import {color} from '../../../../styles/colors';
class ChatContent extends React.PureComponent {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.fetch();
    }

    fetch = () => {
        this.props.fetchMessages(this.props.chatMeta);
    };

    render() {
        return (
            <InfiniteScrollArea
                style={this.props.style}
                fetching={this.props.fetchingMessages}
                anchorToBottom={true}
                onScrollToEnd={this.fetch}
                reversed={true}
                content={
                    this.props.messages && this.props.messages.map((message, i) =>
                        <Message
                            key={i}
                            me={this.props.user.id === message.user.id}
                            icon={
                                <Avatar
                                    size={26}
                                    backgroundColor={color(message.user.fullName[0])}
                                >
                                    {message.user.firstName[0]+message.user.lastName[0]}
                                </Avatar>
                            }
                            message={message}
                        />
                    )
                }
            />
        );
    }
}

export default connect(
    (state) => {
        return {
            chat: currentChat(state) || {},
            chatMeta: currentChatMessagesMeta(state) || {},
            fetchingMessages: state.app.chat.fetchingMessages,
            messages: currentChatMessages(state),
            user: currentUser(state)
        }
    },
    (dispatch) => {
        return {
            fetchMessages: (chatMeta) => {
                if (!chatMeta.last) dispatch(fetchMessages())
            }
        }
    }
)(ChatContent)
