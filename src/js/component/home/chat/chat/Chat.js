import React from 'react';
import {
    Paper,
    Divider
} from 'material-ui';
import {connect} from 'react-redux';
import ChatHeader from './ChatHeader';
import ChatToolbar from './ChatToolbar';
import ChatContent from './ChatContent';
import ChatInput from './ChatInput';
import {ResizeSensor} from 'css-element-queries';

//state
import {currentChat} from '../../../../state/db/chat/chatSelectors';

const chatStyles = {
    content: {position: 'absolute', top: 94, left: 0, right: 0},
    input: {position: 'absolute', bottom: 0, left: 0, right: 0}
};
class Chat extends React.Component {

    constructor(props) {
        super(props);
        this.state = {inputHeight: 40};
    }

    updateInputHeight = () => {
        this.setState({inputHeight: document.getElementById('chatInput').clientHeight})
    };

    componentDidMount() {
        this.updateInputHeight();
        new ResizeSensor(document.getElementById('chatInput'), this.updateInputHeight);
    }

    render() {
        return (
            <Paper
                zDepth={1}
                style={{height: '100%', overflow: 'hidden'}}
            >
                <ChatHeader/>
                <Divider/>
                <ChatToolbar/>
                <Divider/>
                <ChatContent style={{...chatStyles.content, bottom: this.state.inputHeight}}
                />
                <div style={chatStyles.input}>
                    <Divider/>
                    <div id='chatInput'>
                        <ChatInput/>
                    </div>
                </div>
            </Paper>
        );
    }
}

export default connect(
    (state) => {
        return {
            chat: currentChat(state)
        }
    },
    (dispatch) => {
        return {}
    }
)(Chat)

