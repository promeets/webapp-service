import React from 'react';
import {IconButton, IconMenu, ListItem, Toggle} from 'material-ui'
import {
    NavigationArrowBack,
    CommunicationCall,
    SocialGroupAdd,
    ActionToday,
    ContentFilterList,
    ActionSettings
} from 'material-ui/svg-icons';
import {iconStyles} from '../../../../styles/iconStyles';
import {connect} from 'react-redux';
import {closeChat} from '../../../../state/app/chat/chatAppState';
import {currentChat} from '../../../../state/db/chat/chatSelectors';
import {createCall} from '../../../../state/app/call/callAppState';

class ChatToolbar extends React.Component {
    render() {
        return (
            <div style={{textAlign: 'center'}}>
                <IconButton
                    onClick={this.props.onBackClick}
                    style={iconStyles.medium}
                    iconStyle={iconStyles.mediumIcon}
                >
                    <NavigationArrowBack/>
                </IconButton>
                <IconButton
                  onClick={this.props.onCallClick}>
                    <CommunicationCall/>
                </IconButton>
                <IconButton>
                    <SocialGroupAdd/>
                </IconButton>
                <IconButton>
                    <ActionToday/>
                </IconButton>
                <IconButton>
                    <ContentFilterList/>
                </IconButton>
                <IconMenu
                    iconButtonElement={<IconButton><ActionSettings/></IconButton>}
                    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                    targetOrigin={{horizontal: 'right', vertical: 'top'}}
                >
                    <ListItem
                        style={{paddingTop: 14, paddingBottom: 12, fontSize: 14}}
                        rightToggle={<Toggle/>}
                        primaryText={"Notifications"}
                    />
                    <ListItem
                        style={{paddingTop: 14, paddingBottom: 12, fontSize: 14}}
                        rightToggle={<Toggle/>}
                        primaryText={"Quick access"}
                    />
                </IconMenu>
            </div>
        );
    }
}
export default connect(
    (state) => {
        return {
            currentChat: currentChat(state)
        }
    },
    (dispatch) => {
        return {
            onBackClick: () => dispatch(closeChat()),
            onCallClick: () => dispatch(createCall())
        }
    }
)(ChatToolbar)
