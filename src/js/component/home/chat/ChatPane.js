import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {
    CircularProgress,
    Paper
} from 'material-ui'
import SplitPane from 'react-split-pane';
import ChatList from './chats/ChatList';
import ChatBookmarks from './bookmarks/ChatBookmarks';
import Chat from './chat/Chat';
import {connect} from 'react-redux';
import {initChat} from '../../../state/app/chat/chatAppState';
import Loading from '../../common/Loading';
const style = {
    center: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        margin: 'auto'
    }
};
class ChatPane extends React.Component {

    componentDidMount() {
        this.props.initChat();
    }

    render() {
        return this.props.init ?
            <SplitPane
                split="vertical"
                defaultSize={36}
                primary="second"
                allowResize={true}
                style={{paddingLeft: 1}}
            >
                {this.props.init && (
                    this.props.isChatViewOpened ?
                        (this.props.chatId ?
                                <Chat/>
                                :
                                <ChatList/>
                        )
                        :
                        ("")
                )}
                <ChatBookmarks/>
            </SplitPane>

            :

            <Paper
                zDepth={1}
                style={style.center}
            >
                <Loading/>
            </Paper>
    }
}

export default connect(
    (state) => {
        return {
            init: state.app.chat.init,
            chatId: state.app.chat.chatId,
            isChatViewOpened: state.app.chat.opened
        }
    },
    (dispatch) => {
        return {
            initChat: () => dispatch(initChat())
        }
    }
)
(muiThemeable()(ChatPane));
