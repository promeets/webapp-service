import React from 'react';
import {connect} from 'react-redux';
import {sendMessage} from '../../../../state/app/chat/messagesState';
import {
    CommunicationChat
} from 'material-ui/svg-icons'
import {
    FlatButton
} from 'material-ui'
class MentionInChat extends React.Component {

    componentWillReceiveProps(nextProps) {
        try {
            nextProps.action && JSON.stringify(nextProps.action)
        } catch (e) {
            console.error("Action must be plain object");
            console.error(e);
        }
    }

    sendMessage = () => {
        if (!this.props.chatId) return;
        this.props.sendMessage({
            text: this.props.text,
            action: JSON.stringify(this.props.action),
            chatId: this.props.chatId,
            time: new Date(),
            icon: this.props.icon
        })
    };

    render() {
        return (
            <FlatButton
                disabled={!this.props.chatId}
                primary={true}
                label={"Mention"}
                onClick={this.sendMessage}
                tooltip={this.props.toolTip}
                icon={<CommunicationChat/>}
            />
        );
    }
}
MentionInChat.propTypes = {
    text: React.PropTypes.string,
    description: React.PropTypes.string,
    action: React.PropTypes.object,
    toolTip: React.PropTypes.string,
    icon: React.PropTypes.string
};

export default connect(
    (state) => {
        return {
            chatId: state.app.chat.chatId
        }
    },
    (dispatch) => {
        return {
            sendMessage: (message) => {
                dispatch(sendMessage(message))
            }
        }
    }
)(MentionInChat)