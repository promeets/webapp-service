import React from 'react';
import CssStyle from './message-label.scss';

class MessageLabel extends React.Component {
    render() {
        return (
            <div className="message-label">
                    <span className="message-label-icon"
                          style={{background: this.props.color}}/>
                <span className="message-label-text">{this.props.name}</span>
            </div>
        )
    }
}
MessageLabel.propTypes = {
    name: React.PropTypes.string,
    color: React.PropTypes.string
};
export default MessageLabel;