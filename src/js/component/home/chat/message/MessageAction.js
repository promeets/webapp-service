import React from 'react';
import PropTypes from 'prop-types';
import {FontIcon} from "material-ui";
import CssStyle from './messageStyles.scss'

const actionStyles = {
    box: {
        position: 'relative',
        cursor: 'pointer'
    },
    text: {
        marginLeft: 20,
        paddingTop: 4,
        paddingLeft: 10,
        paddingBottom: 4,
        color: '#1B5E20'
    },
    icon: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        margin: 'auto',
        color: '#1B5E20',
        height: 24
    }
};
const DEFAULT_ICON = "info outline";
export default class MessageAction extends React.Component {
    render() {
        return (
            <div
                style={actionStyles.box}
                onClick={this.props.onClick}
            >
                <FontIcon
                    style={actionStyles.icon}
                    className="material-icons"
                >
                    {this.props.icon || DEFAULT_ICON}
                </FontIcon>
                <div
                    style={actionStyles.text}
                    className="message-action-text"
                >
                    {this.props.text}
                </div>
            </div>
        )
    }
}

MessageAction.propTypes = {
    icon: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.function
};