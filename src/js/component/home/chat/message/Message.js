import React from 'react';
import moment from 'moment';
import {connect} from "react-redux";
import MessageAction from "./MessageAction";

const timeFormat = 'HH:mm';
const messageStyles = {
    box: {
        position: 'relative',
        marginTop: 6,
        marginLeft: 6,
        marginRight: 40,
        marginBottom: 4,
    },
    meBox: {
        marginLeft: 40,
        marginRight: 4
    },
    icon: {
        position: 'absolute',
        top: 5,
        left: 6
    },
    textBox: {
        padding: 8,
        marginLeft: 38,
        borderRadius: 4,
        backgroundColor: '#F5F5F5'
    },
    meTextBox: {
        backgroundColor: '#E8F5E9'
    },
    text: {
        fontSize: 13,
        whiteSpace: 'pre-line',
        wordWrap: 'break-word'
    },
    headerBox: {
        marginLeft: 38,
    },
    nameText: {
        fontSize: 12
    },
    timeText: {
        fontSize: 10,
        color: '#9E9E9E'
    }
};
class Message extends React.Component {

    constructor(props) {
        super(props);
    }

    dispatch = () => {
        this.props.dispatch(this.props.message.action)
    };

    render() {
        const boxStyle = {
            ...messageStyles.box,
            ...this.props.me ? messageStyles.meBox : {},
            marginTop: this.props.message.hideHeader ? -10 : messageStyles.box.marginTop
        };
        const textBoxStyle = {
            ...messageStyles.textBox,
            ...this.props.me ? messageStyles.meTextBox : {}
        };
        const textStyle = messageStyles.text;
        const icon = (
            !this.props.me
            && !this.props.message.hideHeader
            && <div style={messageStyles.icon}>{this.props.icon}</div>
        );
        const headerText = (
            !this.props.message.hideHeader &&
            <div>
                {
                    !this.props.me &&
                    <span style={messageStyles.nameText}>{`${this.props.message.user.firstName} `}</span>
                }
                <span style={messageStyles.timeText}>{moment(this.props.message.time).format(timeFormat)}</span>
            </div>
        );
        const text = (
            this.props.message.action ?
                <MessageAction
                    text={this.props.message.text}
                    icon={this.props.message.icon}
                    onClick={this.dispatch}
                />
                :
                this.props.message.text
        );
        return (
            <div style={boxStyle}>
                {icon}
                <div style={messageStyles.headerBox}>
                    {headerText}
                </div>
                <div style={textBoxStyle}>
                    <div style={textStyle}>
                        {text}
                    </div>
                </div>
            </div>
        );
    }
}
Message.propTypes = {
    icon: React.PropTypes.node,
    message: React.PropTypes.object,
    me: React.PropTypes.bool
};


export default connect(
    (state) => {
        return {}
    },
    (dispatch) => {
        return {
            dispatch: (action) => {
                dispatch(action);
            }
        }
    }
)(Message)


