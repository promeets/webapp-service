import React from 'react';
import {Paper, Checkbox, List, ListItem, Avatar} from 'material-ui';
import MessageIcon from 'material-ui/svg-icons/communication/message';
import ScrollArea from '../../../common/scrollbar/ScrollArea';
import {connect} from 'react-redux';
import {toggleChatView} from '../../../../state/app/chat/chatViewState';
import {openChat} from '../../../../state/app/chat/chatAppState';
import {color} from '../../../../styles/colors';
class QuickAccessChatBar extends React.Component {

    componentDidMount() {
        this.props.onComponentDidMount();
    }

    render() {
        return (
            <Paper zDepth={2}
                   className="chat-quick-access-bar"
                   style={{height: '100%'}}>
                <ScrollArea
                    style={{position: 'absolute', top: 0, bottom: 0, left: 0, right: 0}}
                    hideScrollbar={true}
                >
                    <Checkbox
                        style={{margin: '12px 0 0 6px'}}
                        onClick={this.props.onChatToggleButtonClick}
                        checkedIcon={<MessageIcon/>}
                        uncheckedIcon={<MessageIcon/>}
                        defaultChecked={this.props.isChatViewOpened}
                    />
                    <List>
                        {this.props.bookmarks.map((chat, i) =>
                            <ListItem
                                key={i}
                                onClick={() => this.props.onOpenChat(chat)}
                                leftAvatar={
                                    <Avatar
                                        style={{left: 5, top: 7}}
                                        size={26}
                                        backgroundColor={color(chat.name[0])}
                                    >
                                        {chat.name[0]}
                                    </Avatar>
                                }
                            />
                        )}
                    </List>
                </ScrollArea>
            </Paper>
        );
    }
}

export default connect(
    (state) => {
        return {
            bookmarks: [],
            chat: {},
            isChatViewOpened: state.app.chat.opened
        }
    },
    (dispatch) => {
        return {
            onComponentDidMount: () => {
                //dispatch()
            },
            onChatToggleButtonClick: () => {
                dispatch(toggleChatView())
            },
            onOpenChat: (chat) => {
                dispatch(openChat(chat))
            }
        }
    }
)(QuickAccessChatBar)