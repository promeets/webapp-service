import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import muiThemeable from 'material-ui/styles/muiThemeable';
import HeaderContainer from './header/HeaderContainer';
import {Chat} from '../../component/home/chat/promeets-chat';
import SplitPane from 'react-split-pane';
import ProjectList from './projectList/ProjectList';
import CallRequestSnackbar from '../../component/home/callRequest/CallRequestSnackbar';


class Home extends React.Component {
    render() {
        return (
            <SplitPane
                split="vertical"
                defaultSize={this.props.isChatOpened ? 400 : 36}
                minSize={0}
                primary="second"
                allowResize={true}>
                <div style={{height: '100%'}}>
                    <HeaderContainer/>
                    <Drawer open={this.props.header.isDrawerOpen}>
                        <MenuItem onClick={this.props.onCompanyItemClick}>Company</MenuItem>
                        <MenuItem onClick={this.props.onDashboardItemClick}>Dashboard</MenuItem>
                        <MenuItem onClick={this.props.onCalendarItemClick}>Calendar</MenuItem>
                        <MenuItem onClick={this.props.onProjectsItemClick}>Projects</MenuItem>
                    </Drawer>
                    {this.props.children}
                    <CallRequestSnackbar/>
                </div>
                <Chat/>
            </SplitPane>
        )
    }
}

export default muiThemeable()(Home)
