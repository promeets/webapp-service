import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';

const styles = {
    container: {
        background: "#eeeeee",
        height: "100%",
        fontSize: '32px',
        textAlign: 'center'
    }
}

class Dashboard extends React.Component {
    render() {
        return (
            <div style={styles.container}>Dashboard</div>
        )
    }
}

export default muiThemeable()(Dashboard)