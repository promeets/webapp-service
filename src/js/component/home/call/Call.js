import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import CircularProgress from 'material-ui/CircularProgress';
import FloatingActionButton from 'material-ui/FloatingActionButton';

const styles = {
    container: {
        height: "100%",
        margin: "10px"
    },
    hangUpButton: {
        position: "absolute",
        bottom: "20px",
        right: "20px"
    },
    header: {
        fontSize: '32px',
        textAlign: 'center'
    },
    progress: {
        display: 'block',
        margin: 'auto'
    },
    video: {
        margin: '4px',
        height: '100%',
        width: '480px'
    },
    videoContainer: {
        display: 'inline'
    }
}

class Call extends React.Component {

    getRemoteVideosMap() {
        let remoteVideosMap = {};
        Object.keys(this.refs)
          .filter(key => key.startsWith('remoteVideo'))
          .map(key => remoteVideosMap[key.split('_')[1]] = this.refs[key]);
        return remoteVideosMap;
    }

    init() {
        if(this.props.call.creator)
            this.props.onCreateCall(this.props.user.id,
                this.props.participantIds,
                this.refs.localVideo);
        else
            this.props.onOpenCall(this.props.user.id,
              this.props.call.activeParticipants,
              this.refs.localVideo,
              this.getRemoteVideosMap());
    }

    componentDidMount() {
        if(this.props.participantIds.length > 0)
            this.init();
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.participantIds.length === 0 && this.props.participantIds.length > 0)
            this.init();
        if(prevProps.call.activeParticipants.length < this.props.call.activeParticipants.length)
            this.props.onNewParticipantsConnected(
                this.props.call.activeParticipants.filter(participantId =>
                    !prevProps.call.activeParticipants.includes(participantId)),
                this.getRemoteVideosMap()
            );
        if(prevProps.call.activeParticipants.length > this.props.call.activeParticipants.length)
            this.props.onParticipantsDisconnected(
                prevProps.call.activeParticipants.filter(participantId =>
                    !this.props.call.activeParticipants.includes(participantId))
            );
    }

    componentWillUnmount() {
        this.props.onHangUp(this.props.call.id, this.refs.localVideo, this.getRemoteVideosMap());
    }

    render() {
        return(
            <div style={styles.container}>
            { this.props.participantIds.length === 0 && <CircularProgress style={styles.progress}/>}
            { this.props.participantIds.length > 0 &&
                <div>
                    <div style={styles.videoContainer}>
                        <video style={styles.video} id="localVideo" ref="localVideo" controls autoPlay></video>
                    </div>
                    {this.props.call.activeParticipants
                        .filter(participantId => participantId != this.props.user.id)
                        .map(participantId =>
                            <div style={styles.videoContainer} key={`remoteVideoContainer_${participantId}`}>
                                <video style={styles.video} ref={`remoteVideo_${participantId}`} controls autoPlay></video>
                            </div>
                    )}
                    <FloatingActionButton
                        secondary={true}
                        style={styles.hangUpButton}
                        onClick={()=>this.props.redirectFromCall()}
                    >
                        <i className="material-icons">call_end</i>
                    </FloatingActionButton>
                </div>
            }
            </div>
        )
    }
}

export default muiThemeable()(Call)
