import React from 'react';
import {
  connect
} from 'react-redux';
import {
  Observable
} from 'rxjs/Observable';
import Call from './Call';
import WebRtcService from '../../../service/signaling/WebRtcService';
import SignalingService from '../../../service/signaling/SignalingService';
import {sendOffer, sendAnswer, sendLeave, hangUp, redirectFromCall} from '../../../state/app/call/callAppState';
import {sendCallRequest} from '../../../state/app/call/callRequestAppState';
import {currentChat} from '../../../state/db/chat/chatSelectors';


const mapDispatchToProps = (dispatch) => {
    return {
        onOpenCall: (currentUserId, activeParticipants, localVideo, remoteVideos) => {
            WebRtcService.getUserMedia(localVideo)
                .flatMap((stream) => WebRtcService.createConnections(dispatch, activeParticipants, remoteVideos))
                .flatMap(({participantId, connection}) => WebRtcService.createOffer(participantId, connection))
                .subscribe(({participantId, offer}) => dispatch(sendOffer(participantId, offer)),
                    ex => console.log(ex),
                    () => console.log('completed'));
        },
        onCreateCall: (currentUserId, participantIds, localVideo) => {
            const participantsExceptCurrentUser =
                participantIds.filter(participantId => participantId != currentUserId);
            WebRtcService.getUserMedia(localVideo).subscribe(stream => console.log("stream is attached" + stream), error => console.log(error));
            participantsExceptCurrentUser.map(participantId => dispatch(sendCallRequest(participantId)));
        },
        onNewParticipantsConnected: (participantIds, remoteVideosMap) => {
            participantIds.map(participantId => {
                WebRtcService.setOnAddStreamOnCandidateCallback(participantId, dispatch, remoteVideosMap[participantId])
                    .flatMap(({participantId, connection}) => WebRtcService.createAnswerForParticipant(participantId))
                    .subscribe(({participantId, answer}) => dispatch(sendAnswer(participantId, answer)));
            });
        },
        onParticipantsDisconnected: (participantIds) => {
            participantIds.map(participantId => {
                WebRtcService.disconnectFromParticipant(participantId);
            });
        },
        onHangUp: (chatId, localVideo, remoteVideos) => {
            WebRtcService.hangUp(localVideo, remoteVideos);
            Object.keys(remoteVideos).map((participantId) => dispatch(sendLeave(participantId)));
            SignalingService.unsubscribeFromCall(chatId);
            //dispatch(hangUp());
        },
        redirectFromCall: () => {
            dispatch(redirectFromCall());
        }
    }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    call: state.app.call,
    participantIds: state.app.chat.chatId ? currentChat(state).participants.map(participant => participant.userId) : [],
    participants: state.app.chat.chatId ? currentChat(state).participants : []
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Call)
