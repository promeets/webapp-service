import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {callRequestTimeout} from '../../../state/app/call/callRequestAppState'
import {answerCall} from '../../../state/app/call/callRequestAppState'
import {connect} from 'react-redux';

class CallRequestSnackbar extends React.Component {

    render() {
        return(
            <Snackbar
                open={this.props.callRequest.requiresAnswer}
                message="Someone is calling you"
                action="Answer"
                autoHideDuration={20000}
                onRequestClose={reason => {
                    if(reason === "timeout")
                        this.props.onTimeoutExpired()
                    }}
                onActionTouchTap={() => this.props.onAnswer()}
            />
        )
    }
}

export default connect(
    (state) => {
        return {
            callRequest: state.app.callRequest
        }
    },
    (dispatch) => {
        return {
            onTimeoutExpired: () => dispatch(callRequestTimeout()),
            onAnswer: () => dispatch(answerCall())
        }
    }
)(muiThemeable()(CallRequestSnackbar))
