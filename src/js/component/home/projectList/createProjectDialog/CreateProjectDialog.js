import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Formsy from 'formsy-react';
import { FormsyText } from 'formsy-material-ui/lib';

const styles = {
    container: {
        margin: "10px",
        height: "100%"
    },
    form: {
        width: "300px"
    },
    dialog: {
        width: "350px"
    },
    textField: {
        color: "#212121"
    },
    progress: {
        display: 'block',
        margin: 'auto'
    },
    button: {
        marginTop: "10px"
    },
    actionsBar: {
        float: "right",
        marginTop: "16px"
    }
};

class CreateProjectDialog extends React.Component {
    handleClose() { this.props.toggleDialog(false) }

    onValidSubmit(data) {
        this.props.createProject(data)
    }

    render() {
        return (
            <Dialog
                title="Create project"
//                actions={actions}
                modal={false}
                open={this.props.isDialogOpen}
                contentStyle={styles.dialog}
                bodyStyle={{padding: "0 24px 16px"}}
                onRequestClose={() => this.handleClose()}
            >
                <div>
                    <Formsy.Form
                        onValidSubmit={this.onValidSubmit.bind(this)}
                        style={styles.form}
                    >
                        <FormsyText
                            name="name"
                            ref="name"
                            required
                            floatingLabelText={<span>Name</span>}
                            inputStyle={styles.textField}
                        />
                        <FormsyText
                            name="description"
                            ref="description"
                            floatingLabelText={<span>Description</span>}
                            multiLine={true}
                            rowsMax={5}
                            inputStyle={styles.textField}
                        />
                        <div style={styles.actionsBar}>
                            <FlatButton
                                label="Cancel"
                                primary={true}
                                onTouchTap={() => this.handleClose()}
                            />
                            <FlatButton
                                label="Create"
                                type="submit"
                                primary={true}
                            />
                        </div>
                    </Formsy.Form>
                </div>
            </Dialog>
        )
    }
}

export default muiThemeable()(CreateProjectDialog)