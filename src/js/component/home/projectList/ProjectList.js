import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import ProjectListItem from './projectListItem/ProjectListItem';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import {GridList} from 'material-ui/GridList';
import CreateProjectDialog from './createProjectDialog/CreateProjectDialog';
import Loading from "../../common/Loading";

const styles = {
    container: {
        height: "100%",
        margin: "10px"
    },
    addButton: {
        position: "absolute",
        bottom: "20px",
        right: "20px"
    },
    header: {
        fontSize: '30px',
        textAlign: 'left',
        padding: 10
    },
    progress: {
        display: 'block',
        margin: 'auto'
    }
};

class ProjectList extends React.Component {

    componentWillMount() {
        if (this.props.user.links.projects)
            this.props.fetchProjectList(this.props.user);
    }

    render() {
        return (
            this.props.user.links.projects ? this.renderProjectsList() : this.renderEmptyProjectsList()
        )
    }

    renderProjectsList() {
        return (
            <div style={styles.container}>
                <div style={styles.header}>Projects</div>
                { this.props.projects.isFetching && <Loading/>}
                { !this.props.projects.isFetching &&
                <div>
                    <GridList
                        cellHeight={180}
                        cols={4}
                    >
                        {this.props.projects.projectList.map((projectListItem) =>
                            <ProjectListItem
                                href={projectListItem.href}
                                key={projectListItem.projectId}
                                projectId={projectListItem.projectId}
                                name={projectListItem.projectName}
                                onProjectListItemClick={this.props.onProjectListItemClick}
                            />
                        )}
                    </GridList>
                    <FloatingActionButton
                        secondary={true}
                        style={styles.addButton}
                        onClick={() => this.props.toggleCreateProjectDialog(true)}
                    >
                        <i className="material-icons">add</i>
                    </FloatingActionButton>
                    <CreateProjectDialog
                        toggleDialog={this.props.toggleCreateProjectDialog}
                        isDialogOpen={this.props.projects.isDialogOpen}
                        createProject={(project) =>
                            this.props.createProject(project, this.props.user)}
                    />
                </div>
                }
            </div>
        )
    }

    renderEmptyProjectsList() {
        return (
            <div style={styles.header}>There are no projects associated with you.</div>
        )
    }
}

export default muiThemeable()(ProjectList)
