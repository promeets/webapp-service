import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {GridTile} from 'material-ui/GridList';
import {Link} from 'react-router'
import randomColor from 'randomcolor';
import {color} from '../../../../styles/colors';
import {Paper} from "material-ui";
const styles = {
    container: {
        background: "#eeeeee",
        height: "100%",
        fontSize: '32px',
        textAlign: 'center'
    }
};

class ProjectListItem extends React.Component {
    render() {
        return (
            <GridTile
                key={this.props.key}
                title={this.props.name}
                titlePosition="top"
                titleBackground={'rgba(0, 0, 0, 0)'}
                titleStyle={{fontSize: "24px"}}
                style={{
                    background: color(this.props.name[0]),
                    cursor: 'pointer'
                }}
                onClick={() => this.props.onProjectListItemClick(this.props.projectId)}
            >
            </GridTile>
        )
    }
}

export default muiThemeable()(ProjectListItem)