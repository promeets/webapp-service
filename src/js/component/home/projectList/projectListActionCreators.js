import Rest from 'rest';
import {RESOURCE_API_PATH} from '../../../api/apiPaths';
import csrf from 'rest/interceptor/csrf'
import CookiesService from '../../../service/CookiesService'

export const PROJECT_LIST_REQUEST = "PROJECT_LIST_REQUEST";
export const PROJECT_LIST_SUCCESS = "PROJECT_LIST_SUCCESS";
export const PROJECT_LIST_ERROR = "PROJECT_LIST_ERROR";
export const TOGGLE_CREATE_PROJECT_DIALOG = "TOGGLE_CREATE_PROJECT_DIALOG";
export const CREATE_PROJECT_REQUEST = "CREATE_PROJECT_REQUEST";
export const CREATE_PROJECT_SUCCESS = "CREATE_PROJECT_SUCCESS";
export const CREATE_PROJECT_ERROR = "CREATE_PROJECT_ERROR";


function createProjectListSuccessAction(response) {
    const entity = JSON.parse(response.entity);
    return {
        type: PROJECT_LIST_SUCCESS,
        projectList: entity._embedded['user-projects']
    }
}

function createProjectListErrorAction() {
    return {
        type: PROJECT_LIST_ERROR,
        error: "Error during fetching project list."
    }
}

export function createFetchProjectListAction(user) {
    return (dispatch) => {
        dispatch({ type: PROJECT_LIST_REQUEST });
        return Rest({
            path: user.links.projects.href,
        }).then((response) => {
                if(response.status.code === 200) {
                    dispatch(createProjectListSuccessAction(response));
                } else {
                    dispatch(createProjectListErrorAction());
                }
            },
            (error) => {
                dispatch(createProjectListErrorAction());
            }
        );
    }
}

export function createToggleCreateProjectDialogAction(isOpen) {
    return {
        type: TOGGLE_CREATE_PROJECT_DIALOG,
        isOpen
    }
}

function createProjectErrorAction() {
    return {
        type: CREATE_PROJECT_ERROR,
        error: "Error during creation project."
    }
}

export function createProjectAction(project, user) {
    return (dispatch) => {
        dispatch({ type: CREATE_PROJECT_REQUEST });
        return Rest.wrap(csrf, {token: CookiesService.getCookie(CookiesService.XSRF_TOKEN), name: "X-XSRF-TOKEN"})({
            method: "POST",
            path: RESOURCE_API_PATH + "/projects",
            entity: JSON.stringify(project),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
                if(response.status.code === 201) {
                    dispatch(createFetchProjectListAction(user))
                    dispatch(createToggleCreateProjectDialogAction(false));
                } else {
                    dispatch(createProjectErrorAction());
                }
            },
            (error) => {
                dispatch(createProjectErrorAction());
            }
        );
    }
}
