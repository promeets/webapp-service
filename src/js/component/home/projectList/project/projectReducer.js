import projectInfoReducer from "./projectInfo/projectInfoReducer"
import taskListReducer from "./taskList/taskListReducer"
import projectMembersReducer from "./members/projectMembersReducer"

const INITIAL_STATE = {
    projectInfo: {
        isFetching: true,
        isEditing: false,
        isSaving: false
    },
    projectTaskList: {
        isFetching: true,
        isDialogOpen: false
    },
    projectMembers: {
        isFetching: true,
        isDialogOpen: false
    }
};

function projectReducer(state = INITIAL_STATE, action) {
    return {
        projectInfo: projectInfoReducer(state.projectInfo, action),
        projectTaskList: taskListReducer(state.projectTaskList, action),
        projectMembers: projectMembersReducer(state.projectMembers, action)
    }
}

export default projectReducer
