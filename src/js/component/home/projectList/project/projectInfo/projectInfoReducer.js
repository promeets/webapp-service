import {REQUEST_PROJECT_INFO,
    REQUEST_PROJECT_INFO_SUCCESS,
    REQUEST_PROJECT_INFO_ERROR,
    SAVE_PROJECT_INFO,
    SAVE_PROJECT_INFO_SUCCESS,
    SAVE_PROJECT_INFO_ERROR,
    TOGGLE_PROJECT_INFO_EDIT_MODE} from "./projectInfoActionCreators"


function projectInfoReducer(state, action) {
    switch (action.type) {
        case REQUEST_PROJECT_INFO_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                projectEntity: action.project
            });
        case REQUEST_PROJECT_INFO_ERROR:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error
            });
        case REQUEST_PROJECT_INFO:
            return Object.assign({}, state, {
                isFetching: true
            });
        case SAVE_PROJECT_INFO_SUCCESS:
            return Object.assign({}, state, {
                isSaving: false,
                projectEntity: action.project
            });
        case SAVE_PROJECT_INFO_ERROR:
            return Object.assign({}, state, {
                isSaving: false,
                error: action.error
            });
        case SAVE_PROJECT_INFO:
            return Object.assign({}, state, {
                isSaving: true,
            });
        case TOGGLE_PROJECT_INFO_EDIT_MODE:
            return Object.assign({}, state, {
                isEditing: !state.isEditing
            });
        default:
            return state
    }
}

export default projectInfoReducer
