import Rest from 'rest';
import csrf from 'rest/interceptor/csrf'
import CookiesService from '../../../../../service/CookiesService'
import {RESOURCE_API_PATH} from '../../../../../api/apiPaths';

export const TOGGLE_PROJECT_INFO_EDIT_MODE = "TOGGLE_PROJECT_INFO_EDIT_MODE";

export const REQUEST_PROJECT_INFO = "REQUEST_PROJECT_INFO";
export const REQUEST_PROJECT_INFO_SUCCESS = "REQUEST_PROJECT_INFO_SUCCESS";
export const REQUEST_PROJECT_INFO_ERROR = "REQUEST_PROJECT_INFO_ERROR";

export const SAVE_PROJECT_INFO = "SAVE_PROJECT_INFO";
export const SAVE_PROJECT_INFO_SUCCESS = "SAVE_PROJECT_INFO_SUCCESS";
export const SAVE_PROJECT_INFO_ERROR = "SAVE_PROJECT_INFO_ERROR";


export function createProjectInfoToggleEditModeAction() {
    return {type: TOGGLE_PROJECT_INFO_EDIT_MODE}
}

function createRequestProjectInfoSuccessAction(response) {
    const entity = JSON.parse(response.entity);
    return {
        type: REQUEST_PROJECT_INFO_SUCCESS,
        project: entity
    }
}

function createRequestProjectInfoErrorAction() {
    return {
        type: REQUEST_PROJECT_INFO_ERROR,
        error: "Error during fetching project info."
    }
}

function createSaveProjectInfoSuccessAction(response) {
    const entity = JSON.parse(response.entity);
    return {
        type: SAVE_PROJECT_INFO_SUCCESS,
        project: entity
    }
}

function createSaveProjectInfoErrorAction() {
    return {
        type: SAVE_PROJECT_INFO_ERROR,
        error: "Error during saving project info."
    }
}

export function createFetchProjectInfoAction(id) {
    return (dispatch) => {
        dispatch({type: REQUEST_PROJECT_INFO});
        return Rest({
            path: RESOURCE_API_PATH + '/projects/' + id,
        }).then((response) => {
                if (response.status.code === 200) {
                    dispatch(createRequestProjectInfoSuccessAction(response));
                } else {
                    dispatch(createRequestProjectInfoErrorAction());
                }
            },
            (error) => {
                dispatch(createRequestProjectInfoErrorAction());
            }
        );
    }
}

export function createSaveProjectInfoAction(href, data) {
    return (dispatch) => {
        dispatch({type: SAVE_PROJECT_INFO});
        return Rest.wrap(csrf, {token: CookiesService.getCookie(CookiesService.XSRF_TOKEN), name: "X-XSRF-TOKEN"})({
            method: "PUT",
            path: href,
            entity: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
                dispatch(createProjectInfoToggleEditModeAction());
                if (response.status.code === 200) {
                    dispatch(createSaveProjectInfoSuccessAction(response));
                } else {
                    dispatch(createSaveProjectInfoErrorAction());
                }
            },
            (error) => {
                dispatch(createProjectInfoToggleEditModeAction());
                dispatch(createSaveProjectInfoErrorAction());
            }
        );
    }
}
