import React from 'react';
import Formsy from 'formsy-react';
import {FormsyText} from 'formsy-material-ui/lib';
import RaisedButton from 'material-ui/RaisedButton';
import Loading from "../../../../common/Loading";

const styles = {
    container: {
        margin: "10px",
        height: "100%"
    },
    form: {
        width: "300px"
    },
    textField: {
        color: "#212121"
    },
    progress: {
        display: 'block',
        margin: 'auto'
    },
    button: {
        marginTop: "10px"
    }
}

class ProjectInfo extends React.Component {

    onValidSubmit(data) {
        this.props.onSaveButtonClick(this.props.projectInfo.projectEntity._links.self.href, data);
    }

    render() {
        return (
            <div style={styles.container}>
                <Formsy.Form
                    style={styles.form}
                    onValidSubmit={this.onValidSubmit.bind(this)}
                >
                    <FormsyText
                        name="name"
                        ref="name"
                        required
                        floatingLabelText={<span>Name</span>}
                        inputStyle={styles.textField}
                        defaultValue={this.props.projectInfo.projectEntity.name}
                        disabled={!this.props.projectInfo.isEditing}
                    />
                    <FormsyText
                        name="description"
                        ref="description"
                        floatingLabelText={<span>Description</span>}
                        multiLine={true}
                        rowsMax={5}
                        textareaStyle={styles.textField}
                        disabled={!this.props.projectInfo.isEditing}
                        value={this.props.projectInfo.projectEntity.description ?
                            this.props.projectInfo.projectEntity.description : null}
                    />
                    { !this.props.projectInfo.isEditing &&
                    <RaisedButton
                        label="edit"
                        onClick={() => this.props.onEditButtonClick()}
                        primary={true}
                        style={styles.button}
                    />
                    }
                    { this.props.projectInfo.isEditing &&
                    <RaisedButton
                        type="submit"
                        label="save"
                        disabled={this.props.projectInfo.isSaving}
                        primary={true}
                        style={styles.button}
                    />
                    }
                    {this.props.projectInfo.isSaving && <Loading/>}
                </Formsy.Form>
            </div>
        )
    }
}

export default ProjectInfo
