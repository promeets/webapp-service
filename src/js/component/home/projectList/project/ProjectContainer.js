import React from 'react';
import {connect} from 'react-redux'
import {browserHistory} from 'react-router'
import Project from './Project'
import {createFetchProjectInfoAction,
    createProjectInfoToggleEditModeAction,
    createSaveProjectInfoAction} from './projectInfo/projectInfoActionCreators'
import {createFetchProjectTaskListAction,
        createToggleCreateTaskDialogAction,
        createTaskAction} from './taskList/taskListActionCreators'
import {fetchProjectMembersAction,
        toggleInviteUserDialogAction,
        inviteUserAction}  from './members/projectMembersActionCreators'

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProjectInfo: (projectId) => {
            dispatch(createFetchProjectInfoAction(projectId))
        },
        fetchProjectTasksList: (tasksHref, usersHref) => {
            dispatch(createFetchProjectTaskListAction(tasksHref, usersHref))
        },
        fetchProjectMembers: (href) => {
            dispatch(fetchProjectMembersAction(href))
        },
        toggleProjectInfoEditMode: () => {
            dispatch(createProjectInfoToggleEditModeAction())
        },
        saveProjectInfo: (href, data) => {
            dispatch(createSaveProjectInfoAction(href, data))
        },
        onTaskClick: (id) => {
            dispatch({type: "ROUTING", to: browserHistory.push("/tasks/" + id)});
        },
        toggleCreateTaskDialog: (isOpen) => {
            dispatch(createToggleCreateTaskDialogAction(isOpen))
        },
        toggleInviteUserDialog: (isOpen) => {
            dispatch(toggleInviteUserDialogAction(isOpen))
        },
        inviteUser: (data, membersHref) => {
            dispatch(inviteUserAction(data, membersHref))
        },
        createTask: (task, taskListHref, usersHref) => {
            dispatch(createTaskAction(task, taskListHref, usersHref))
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        project: state.project
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Project)
