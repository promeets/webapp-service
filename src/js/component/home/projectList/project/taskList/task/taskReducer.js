import {REQUEST_PROJECT_TASK,
    REQUEST_PROJECT_TASK_SUCCESS,
    REQUEST_PROJECT_TASK_ERROR,
    TOGGLE_PROJECT_TASK_EDIT_MODE} from "./taskActionCreators"

const INITIAL_STATE = {isFetching: true, isEditing: false, task: {}};

function projectTaskReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case REQUEST_PROJECT_TASK_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                taskEntity: action.task,
                usersUnderProject: action.usersUnderProject,
                allTaskStatuses: action.allTaskStatuses,
                allTaskPriorities: action.allTaskPriorities
            });
        case REQUEST_PROJECT_TASK_ERROR:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error
            });
        case REQUEST_PROJECT_TASK:
            return Object.assign({}, state, {
                isFetching: true
            });
        case TOGGLE_PROJECT_TASK_EDIT_MODE:
            return Object.assign({}, state, {
                isEditing: !state.isEditing
            });
        default:
            return state
    }
}

export default projectTaskReducer
