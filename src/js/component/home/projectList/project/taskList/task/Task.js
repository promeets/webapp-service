import React from 'react';
import {Card} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import Formsy from 'formsy-react';
import DatePicker from 'material-ui/DatePicker';
import {FormsyText, FormsySelect, FormsyAutoComplete, FormsyDate} from 'formsy-material-ui/lib';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import moment from 'moment';
import {RESOURCE_API_PATH} from '../../../../../../api/apiPaths';

import {push} from 'react-router-redux'
import MentionInChat from '../../../../../../component/home/chat/api/MentionInChat';
import Loading from "../../../../../common/Loading";

const styles = {
    container: {
        margin: "10px",
        height: "100%"
    },
    header: {
        fontSize: "28px",
        width: "100%"
    },
    progress: {
        display: 'block',
        margin: 'auto'
    }
};

const USER_API_PREFIX = RESOURCE_API_PATH + "/users/";

class Task extends React.Component {

    componentWillMount() {
        this.props.fetchTask(this.props.params.taskId);
    }

    onValidSubmit(data) {
        this.props.saveTask(this.props.task.taskEntity._links.self.href, data);
    }

    render() {
        return (
            <div style={styles.container}>
                { this.props.task.isFetching && <Loading/>}
                {
                    !this.props.task.isFetching &&
                    <div style={styles.container}>
                        <Formsy.Form
                            onValidSubmit={this.onValidSubmit.bind(this)}
                        >
                            <FormsyText
                                name="name"
                                ref="name"
                                required
                                disabled={!this.props.task.isEditing}
                                style={styles.header}
                                value={this.props.task.taskEntity.name}
                                inputStyle={{color: "#212121"}}
                                underlineDisabledStyle={{display: "none"}}
                            />
                            <Card
                                style={{paddingBottom: "10px"}}
                            >
                                {!this.props.task.isEditing &&
                                <div style={{padding: 8}}>
                                    <FlatButton
                                        label="Edit"
                                        primary={true}
                                        onClick={() => this.props.toggleProjectTaskEditMode()}
                                        icon={<i className="material-icons" style={styles.hintIcon}>edit</i>}
                                    />
                                    <MentionInChat
                                        text={`${this.props.task.taskEntity.name}`}
                                        action={push("/tasks/" + this.props.task.taskEntity.taskId)}
                                        icon={"assignment"}
                                    />
                                </div>
                                }
                                {this.props.task.isEditing &&
                                <FlatButton
                                    label="Save"
                                    type="submit"
                                    style={{margin: "8px"}}
                                    primary={true}
                                    icon={<i className="material-icons" style={styles.hintIcon}>save</i>}
                                />
                                }
                                <table
                                    style={{width: "100%", margin: "0 10px 0 10px"}}
                                >
                                    <tbody>
                                    <tr>
                                        <td>
                                            <FormsySelect
                                                name="taskPriority"
                                                ref="taskPriority"
                                                floatingLabelText="Priority"
                                                disabled={!this.props.task.isEditing}
                                                value={this.props.task.allTaskPriorities.find(
                                                    (priority) =>
                                                    this.props.task.taskEntity.taskPriority.priorityId === priority.priorityId)._links.self.href}
                                                labelStyle={{color: "#212121"}}
                                            >
                                                {this.props.task.allTaskPriorities.map(
                                                    (taskPriority) =>
                                                        <MenuItem
                                                            value={taskPriority._links.self.href}
                                                            key={taskPriority._links.self.href}
                                                            primaryText={taskPriority.name}
                                                        />
                                                )}
                                            </FormsySelect>
                                        </td>
                                        <td>
                                            <DatePicker
                                                floatingLabelText="Created at"
                                                disabled={true}
                                                value={moment(this.props.task.taskEntity.createdAt).toDate()}
                                                inputStyle={{color: "#212121"}}
                                            />
                                        </td>
                                        <td>
                                            <FormsyText
                                                name="reporter(not for saving)"
                                                floatingLabelText="Reporter"
                                                disabled={true}
                                                inputStyle={{color: "#212121"}}
                                                value={this.props.task.taskEntity.reporter.firstName + " " + this.props.task.taskEntity.reporter.lastName}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <FormsySelect
                                                name="taskStatus"
                                                ref="taskStatus"
                                                floatingLabelText="Status"
                                                disabled={!this.props.task.isEditing}
                                                value={this.props.task.allTaskStatuses.find(
                                                    (status) =>
                                                    this.props.task.taskEntity.taskStatus.statusId === status.statusId)._links.self.href}
                                                labelStyle={{color: "#212121"}}
                                            >
                                                {this.props.task.allTaskStatuses.map(
                                                    (taskStatus) =>
                                                        <MenuItem
                                                            value={taskStatus._links.self.href}
                                                            key={taskStatus._links.self.href}
                                                            primaryText={taskStatus.name}
                                                        />
                                                )}
                                            </FormsySelect>
                                        </td>
                                        <td>
                                            <FormsyDate
                                                name="dueDate"
                                                floatingLabelText="Due date"
                                                disabled={!this.props.task.isEditing}
                                                defaultDate={this.props.task.taskEntity.dueDate ?
                                                    moment(this.props.task.taskEntity.dueDate).toDate() : null}
                                                inputStyle={{color: "#212121"}}
                                            />
                                        </td>
                                        <td>
                                            <FormsySelect
                                                name="assignee"
                                                ref="assignee"
                                                floatingLabelText="Assignee"
                                                disabled={!this.props.task.isEditing}
                                                value={this.props.task.taskEntity.assignee ?
                                                    USER_API_PREFIX + this.props.task.taskEntity.assignee.userId : null}
                                                labelStyle={{color: "#212121"}}
                                            >
                                                <MenuItem
                                                    value={null}
                                                    primaryText={"-"}
                                                />
                                                {this.props.task.usersUnderProject.map(
                                                    (user) =>
                                                        <MenuItem
                                                            value={USER_API_PREFIX + user.userId}
                                                            key={USER_API_PREFIX + user.userId}
                                                            primaryText={user.firstName + " " + user.lastName}
                                                        />
                                                )}
                                            </FormsySelect>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <FormsyText
                                    name="description"
                                    ref="description"
                                    textareaStyle={{color: "#212121"}}
                                    floatingLabelText="Description"
                                    multiLine={true}
                                    rowsMax={5}
                                    disabled={!this.props.task.isEditing}
                                    style={{width: "50%", margin: "0 10px 0 10px"}}
                                    value={this.props.task.taskEntity.description}
                                />


                            </Card>
                        </Formsy.Form>
                    </div>
                }
            </div>
        )
    }
}

export default Task
