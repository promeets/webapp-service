import React from 'react';
import {connect} from 'react-redux'
import Task from './Task'
import {createFetchProjectTaskAction,
        createProjectTaskToggleEditModeAction,
        createSaveProjectTaskAction} from './taskActionCreators'


const mapDispatchToProps = (dispatch) => {
    return {
        fetchTask: (taskId) => {
            dispatch(createFetchProjectTaskAction(taskId))
        },
        toggleProjectTaskEditMode: () => {
            dispatch(createProjectTaskToggleEditModeAction())
        },
        saveTask: (href, data) => {
            dispatch(createSaveProjectTaskAction(href, data))
        }
    }
}


const mapStateToProps = (state) => {
    return {
        task: state.task
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Task)
