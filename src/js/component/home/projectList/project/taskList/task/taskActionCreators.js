import Rest from 'rest';
import csrf from 'rest/interceptor/csrf'
import CookiesService from '../../../../../../service/CookiesService'
import {RESOURCE_API_PATH} from '../../../../../../api/apiPaths';

export const REQUEST_PROJECT_TASK = "REQUEST_PROJECT_TASK";
export const REQUEST_PROJECT_TASK_SUCCESS = "REQUEST_PROJECT_TASK_SUCCESS";
export const REQUEST_PROJECT_TASK_ERROR = "REQUEST_PROJECT_TASK_ERROR";

export const SAVE_PROJECT_TASK = "SAVE_PROJECT_TASK";
export const SAVE_PROJECT_TASK_SUCCESS = "SAVE_PROJECT_TASK_SUCCESS";
export const SAVE_PROJECT_TASK_ERROR = "SAVE_PROJECT_TASK_ERROR";

export const TOGGLE_PROJECT_TASK_EDIT_MODE = "TOGGLE_PROJECT_TASK_EDIT_MODE";


export function createProjectTaskToggleEditModeAction() {
    return {type: TOGGLE_PROJECT_TASK_EDIT_MODE}
}

function createRequestProjectTaskSuccessAction(taskEntity, taskStatuses,
                                               taskPriorities, taskProject) {
    const task = JSON.parse(taskEntity);
    const allTaskStatuses = JSON.parse(taskStatuses)._embedded['task-statuses'];
    const allTaskPriorities = JSON.parse(taskPriorities)._embedded['task-priorities'];
    const usersUnderProject = JSON.parse(taskProject)._embedded.users;
    return {
        type: REQUEST_PROJECT_TASK_SUCCESS,
        task,
        usersUnderProject,
        allTaskStatuses,
        allTaskPriorities
    }
}

function createRequestProjectTaskErrorAction() {
    return {
        type: REQUEST_PROJECT_TASK_ERROR,
        error: "Error during fetching task."
    }
}

function createSaveProjectTaskSuccessAction(response) {
    const entity = JSON.parse(response.entity);
    return {
        type: SAVE_PROJECT_TASK_SUCCESS,
        project: entity
    }
}

function createSaveProjectTaskErrorAction() {
    return {
        type: SAVE_PROJECT_TASK_ERROR,
        error: "Error during saving project task."
    }
}

export function createFetchProjectTaskAction(id) {
    return (dispatch) => {
        dispatch({type: REQUEST_PROJECT_TASK});
        let taskEntityPromise = Rest({
            path: RESOURCE_API_PATH + '/project-tasks/' + id + "?projection=inlineTask",
        });
        let taskStatusesPromise = Rest({
            path: RESOURCE_API_PATH + '/task-statuses/',
        });
        let taskPrioritiesPromise = Rest({
            path: RESOURCE_API_PATH + '/task-priorities/'
        });
        let taskProjectPromise = Rest({
            path: RESOURCE_API_PATH + '/project-tasks/' + id + "/project/"
        });
        return Promise.all([taskEntityPromise, taskStatusesPromise,
            taskPrioritiesPromise, taskProjectPromise]).then((responses) => {
                if (responses.every((response) => response.status.code === 200)) {
                    dispatch(createRequestProjectTaskSuccessAction(responses[0].entity,
                        responses[1].entity, responses[2].entity, responses[3].entity));
                } else {
                    dispatch(createRequestProjectTaskErrorAction());
                }
            },
            (error) => {
                dispatch(createRequestProjectTaskErrorAction());
            }
        );
    }
}

export function createSaveProjectTaskAction(href, data) {
    return (dispatch) => {
        dispatch({type: SAVE_PROJECT_TASK});
        return Rest.wrap(csrf, {token: CookiesService.getCookie(CookiesService.XSRF_TOKEN), name: "X-XSRF-TOKEN"})({
            method: "PATCH",
            path: href,
            entity: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
                dispatch(createProjectTaskToggleEditModeAction());
                if (response.status.code === 200) {
                    dispatch(createSaveProjectTaskSuccessAction(response));
                } else {
                    dispatch(createSaveProjectTaskErrorAction());
                }
            },
            (error) => {
                dispatch(createProjectTaskToggleEditModeAction());
                dispatch(createSaveProjectTaskErrorAction());
            }
        );
    }
}