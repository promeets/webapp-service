import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import moment from 'moment';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import CreateTaskDialog from './createTaskDialog/CreateTaskDialog';
import Loading from "../../../../common/Loading";

const styles = {
    container: {
        margin: "10px",
        height: "100%",
        fontSize: '32px',
        textAlign: 'center'
    },
    addButton: {
        position: "absolute",
        bottom: "110px",
        right: "20px"
    }
};

class TaskList extends React.Component {

    componentWillMount() {
        this.props.fetchProjectTasksList();
    }

    render() {
        return (
            <div style={{height: "100%"}}>
                { this.props.projectTasksList.isFetching && <Loading/>}
                { !this.props.projectTasksList.isFetching &&
                <div style={{height: "100%"}}>
                    <Table
                        selectable={true}
                        onRowSelection={(row) =>
                            this.props.onTaskClick(this.props.projectTasksList.tasksList[row].taskId)}
                    >
                        <TableHeader
                            adjustForCheckbox={false}
                            displaySelectAll={false}
                        >
                            <TableRow>
                                <TableHeaderColumn>Priority</TableHeaderColumn>
                                <TableHeaderColumn>Name</TableHeaderColumn>
                                <TableHeaderColumn>Status</TableHeaderColumn>
                                <TableHeaderColumn>Due Date</TableHeaderColumn>
                                <TableHeaderColumn>Reporter</TableHeaderColumn>
                                <TableHeaderColumn>Assignee</TableHeaderColumn>
                            </TableRow>
                        </TableHeader>
                        <TableBody
                            displayRowCheckbox={false}
                            showRowHover={true}
                        >
                            {
                                this.props.projectTasksList.tasksList.map(
                                    (task) =>
                                        <TableRow
                                            key={task.taskId}
                                        >
                                            <TableRowColumn>{task.taskPriority.name}</TableRowColumn>
                                            <TableRowColumn>{task.name}</TableRowColumn>
                                            <TableRowColumn>{task.taskStatus.name}</TableRowColumn>
                                            <TableRowColumn>{task.dueDate? moment(task.dueDate).format("DD/MM/YYYY") : "-"}</TableRowColumn>
                                            <TableRowColumn>{task.reporter.firstName + " " + task.reporter.lastName}</TableRowColumn>
                                            <TableRowColumn>{task.assignee? task.assignee.firstName + " " + task.assignee.lastName : "-"}</TableRowColumn>
                                        </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                    <FloatingActionButton
                        secondary={true}
                        style={styles.addButton}
                        onClick={()=>this.props.toggleCreateTaskDialog(true)}
                    >
                        <i className="material-icons">add</i>
                    </FloatingActionButton>
                    <CreateTaskDialog
                        toggleDialog={this.props.toggleCreateTaskDialog}
                        isDialogOpen={this.props.projectTasksList.isDialogOpen}
                        taskPriorities={this.props.projectTasksList.tasksPriorities}
                        users={this.props.projectTasksList.usersUnderProject}
                        projectHref={this.props.projectHref}
                        createTask={(task) =>
                            this.props.createTask(task)}
                    />
                </div>
                }
            </div>
        )
    }
}

export default muiThemeable()(TaskList)