import Rest from 'rest';
import csrf from 'rest/interceptor/csrf'
import CookiesService from '../../../../../service/CookiesService'
import {RESOURCE_API_PATH} from '../../../../../api/apiPaths';

export const REQUEST_PROJECT_TASK_LIST = "REQUEST_PROJECT_TASK_LIST";
export const REQUEST_PROJECT_TASK_LIST_SUCCESS = "REQUEST_PROJECT_TASK_LIST_SUCCESS";
export const REQUEST_PROJECT_TASK_LIST_ERROR = "REQUEST_PROJECT_TASK_LIST_ERROR";
export const TOGGLE_CREATE_TASK_DIALOG = "TOGGLE_CREATE_TASK_DIALOG";


function createRequestProjectTaskListSuccessAction(tasks, tasksPriorities, users) {
    return {
        type: REQUEST_PROJECT_TASK_LIST_SUCCESS,
        projectTaskList: JSON.parse(tasks),
        tasksPriorities: JSON.parse(tasksPriorities),
        usersUnderProject: JSON.parse(users)
    }
}

function createRequestProjectTaskListErrorAction() {
    return {
        type: REQUEST_PROJECT_TASK_LIST_ERROR,
        error: "Error during fetching project tasks."
    }
}

export function createFetchProjectTaskListAction(tasksHref, usersHref) {
    return (dispatch) => {
        dispatch({type: REQUEST_PROJECT_TASK_LIST});
        let taskPrioritiesPromise = Rest({
            path: RESOURCE_API_PATH + '/task-priorities/'
        });
        let tasksPromise = Rest({
            path: tasksHref
        });
        let usersPromise = Rest({
            path: usersHref
        });
        return Promise.all([tasksPromise, taskPrioritiesPromise, usersPromise]).then((responses) => {
                if (responses.every((response) => response.status.code === 200)) {
                    dispatch(createRequestProjectTaskListSuccessAction(responses[0].entity,
                        responses[1].entity, responses[2].entity));
                } else {
                    dispatch(createRequestProjectTaskListErrorAction());
                }
            },
            (error) => {
                dispatch(createRequestProjectTaskListErrorAction());
            }
        );
    }
}

export function createToggleCreateTaskDialogAction(isOpen) {
    return {
        type: TOGGLE_CREATE_TASK_DIALOG,
        isOpen
    }
}

function createTaskErrorAction() {
    return {
        type: REQUEST_PROJECT_TASK_LIST_ERROR,
        error: "Error during creation task."
    }
}

export function createTaskAction(task, tasksHref, usersHref) {
    return (dispatch) => {
        //dispatch({ type: CREATE_PROJECT_REQUEST });
        return Rest.wrap(csrf, {token: CookiesService.getCookie(CookiesService.XSRF_TOKEN), name: "X-XSRF-TOKEN"})({
            method: "POST",
            path: RESOURCE_API_PATH + "/project-tasks",
            entity: JSON.stringify(task),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
                if(response.status.code === 201) {
                    dispatch(createFetchProjectTaskListAction(tasksHref, usersHref));
                    dispatch(createToggleCreateTaskDialogAction(false));
                } else {
                    dispatch(createTaskErrorAction());
                }
            },
            (error) => {
                dispatch(createTaskErrorAction());
            }
        );
    }
}
