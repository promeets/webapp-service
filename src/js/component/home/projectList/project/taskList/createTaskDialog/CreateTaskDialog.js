import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Formsy from 'formsy-react';
import { FormsyText, FormsySelect, FormsyDate } from 'formsy-material-ui/lib';
import MenuItem from 'material-ui/MenuItem';
import {RESOURCE_API_PATH} from '../../../../../../api/apiPaths';

const styles = {
    container: {
        margin: "10px",
        height: "100%"
    },
    form: {
        width: "300px"
    },
    dialog: {
        width: "350px"
    },
    textField: {
        color: "#212121"
    },
    progress: {
        display: 'block',
        margin: 'auto'
    },
    button: {
        marginTop: "10px"
    },
    actionsBar: {
        float: "right",
        marginTop: "16px"
    }
};

const USER_API_PREFIX = RESOURCE_API_PATH + "/users/";

class CreateTaskDialog extends React.Component {
    handleClose() { this.props.toggleDialog(false) }

    onValidSubmit(data) {
        this.props.createTask(data)
    }

    render() {
        return (
            <Dialog
                title="Create task"
                modal={false}
                open={this.props.isDialogOpen}
                contentStyle={styles.dialog}
                bodyStyle={{padding: "0 24px 16px"}}
                onRequestClose={() => this.handleClose()}
            >
                <div>
                    <Formsy.Form
                        onValidSubmit={this.onValidSubmit.bind(this)}
                    >
                        <FormsyText
                            name="project"
                            ref="project"
                            style={{display: "none"}}
                            value={this.props.projectHref}
                        />

                        <FormsyText
                            name="name"
                            ref="name"
                            floatingLabelText="Name"
                            required
                        />

                        <FormsySelect
                            name="taskPriority"
                            ref="taskPriority"
                            floatingLabelText="Priority"
                            required
                            value={this.props.taskPriorities.find(
                                (priority) =>
                                3 === priority.priorityId)._links.self.href}
                        >
                            {this.props.taskPriorities.map(
                                (taskPriority) =>
                                    <MenuItem
                                        value={taskPriority._links.self.href}
                                        key={taskPriority._links.self.href}
                                        primaryText={taskPriority.name}
                                    />
                            )}
                        </FormsySelect>
                        <FormsyDate
                            name="dueDate"
                            floatingLabelText="Due date"
                        />
                        <FormsySelect
                            name="assignee"
                            ref="assignee"
                            floatingLabelText="Assignee"
                            value={null}
                        >
                            <MenuItem
                                value={null}
                                primaryText={"-"}
                            />
                            {this.props.users.map(
                                (user) =>
                                    <MenuItem
                                        value={USER_API_PREFIX + user.userId}
                                        key={USER_API_PREFIX + user.userId}
                                        primaryText={user.firstName + " " + user.lastName}
                                    />
                            )}
                        </FormsySelect>
                        <div style={styles.actionsBar}>
                            <FlatButton
                                label="Cancel"
                                primary={true}
                                onTouchTap={() => this.handleClose()}
                            />
                            <FlatButton
                                label="Create"
                                type="submit"
                                primary={true}
                            />
                        </div>
                    </Formsy.Form>
                </div>
            </Dialog>
        )
    }
}

export default muiThemeable()(CreateTaskDialog)