import {REQUEST_PROJECT_TASK_LIST,
    REQUEST_PROJECT_TASK_LIST_SUCCESS,
    REQUEST_PROJECT_TASK_LIST_ERROR,
    TOGGLE_CREATE_TASK_DIALOG} from "./taskListActionCreators"


function taskListReducer(state, action) {
    switch (action.type) {
        case REQUEST_PROJECT_TASK_LIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case REQUEST_PROJECT_TASK_LIST_ERROR:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error
            });
        case REQUEST_PROJECT_TASK_LIST_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                tasksList: action.projectTaskList._embedded["project-tasks"],
                tasksPriorities: action.tasksPriorities._embedded['task-priorities'],
                usersUnderProject: action.usersUnderProject._embedded['user-projects']
            });
        case TOGGLE_CREATE_TASK_DIALOG: {
            return Object.assign({}, state, {
                isDialogOpen: action.isOpen
            });
        }
        default:
            return state
    }
}

export default taskListReducer
