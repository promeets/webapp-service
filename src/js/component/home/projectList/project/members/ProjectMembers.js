import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import InviteUserDialog from './inviteUserDialog/InviteUserDialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';

const styles = {
    container: {
        margin: "10px",
        height: "100%",
        fontSize: '32px',
        textAlign: 'center'
    },
    addButton: {
        position: "absolute",
        bottom: "110px",
        right: "20px"
    }
};

class ProjectMembers extends React.Component {

    render() {
        return (
            <div style={{height: "100%"}}>
                    <List>
                        {this.props.projectMembers.members.map((member) =>
                            <ListItem
                                key={member.userId}
                                primaryText={member.firstName + " " + member.lastName}
                                leftAvatar={<Avatar>{member.firstName[0]}</Avatar>}
                                rightIcon={<i className="material-icons">delete</i>}
                            />
                        )}
                    </List>
                <FloatingActionButton
                    secondary={true}
                    style={styles.addButton}
                    onClick={()=>this.props.toggleInviteUserDialog(true)}
                >
                    <i className="material-icons">add</i>
                </FloatingActionButton>
                <InviteUserDialog
                    toggleDialog={this.props.toggleInviteUserDialog}
                    isDialogOpen={this.props.projectMembers.isDialogOpen}
                    memberIds={this.props.projectMembers.members.map((member)=>member.userId)}
                    allUsers={this.props.projectMembers.allUsers}
                    projectHref={this.props.projectHref}
                    inviteUser={(user) =>
                        this.props.inviteUser(user)}
                />
            </div>
        )
    }
}

export default muiThemeable()(ProjectMembers)