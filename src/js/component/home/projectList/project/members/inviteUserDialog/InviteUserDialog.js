import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Formsy from 'formsy-react';
import { FormsyText, FormsySelect, FormsyDate } from 'formsy-material-ui/lib';
import MenuItem from 'material-ui/MenuItem';
import {RESOURCE_API_PATH} from '../../../../../../api/apiPaths';

const styles = {
    container: {
        margin: "10px",
        height: "100%"
    },
    form: {
        width: "300px"
    },
    dialog: {
        width: "350px"
    },
    textField: {
        color: "#212121"
    },
    progress: {
        display: 'block',
        margin: 'auto'
    },
    button: {
        marginTop: "10px"
    },
    actionsBar: {
        float: "right",
        marginTop: "16px"
    }
};

class InviteUserDialog extends React.Component {
    handleClose() { this.props.toggleDialog(false) }

    onValidSubmit(data) {
        this.props.inviteUser(data)
    }

    render() {
        return (
            <Dialog
                title="Invite user"
                modal={false}
                open={this.props.isDialogOpen}
                contentStyle={styles.dialog}
                bodyStyle={{padding: "0 24px 16px"}}
                onRequestClose={() => this.handleClose()}
            >
                <div>
                    <Formsy.Form
                        onValidSubmit={this.onValidSubmit.bind(this)}
                    >
                        <FormsyText
                            name="project"
                            ref="project"
                            style={{display: "none"}}
                            value={this.props.projectHref}
                        />

                        <FormsyText
                            name="role"
                            ref="role"
                            style={{display: "none"}}
                            value={RESOURCE_API_PATH + "/security-roles/1"}
                        />
                        <FormsySelect
                            name="user"
                            ref="user"
                            floatingLabelText="User"
                            required
                        >
                            {this.props.allUsers.filter((user) =>
                                !this.props.memberIds.includes(user.userId)).map(
                                (user) =>
                                    <MenuItem
                                        value={user._links.self.href}
                                        key={user._links.self.href}
                                        primaryText={user.firstName + " " + user.lastName}
                                    />
                            )}
                        </FormsySelect>
                        <div style={styles.actionsBar}>
                            <FlatButton
                                label="Cancel"
                                primary={true}
                                onTouchTap={() => this.handleClose()}
                            />
                            <FlatButton
                                label="Invite"
                                type="submit"
                                primary={true}
                            />
                        </div>
                    </Formsy.Form>
                </div>
            </Dialog>
        )
    }
}

export default muiThemeable()(InviteUserDialog)