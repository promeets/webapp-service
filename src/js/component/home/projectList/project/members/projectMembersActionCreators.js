import Rest from 'rest';
import csrf from 'rest/interceptor/csrf'
import CookiesService from '../../../../../service/CookiesService'
import {RESOURCE_API_PATH} from '../../../../../api/apiPaths';

export const FETCH_PROJECT_MEMBERS = "FETCH_PROJECT_MEMBERS ";
export const FETCH_PROJECT_MEMBERS_SUCCESS = "FETCH_PROJECT_MEMBERS_SUCCESS";
export const FETCH_PROJECT_MEMBERS_ERROR = "FETCH_PROJECT_MEMBERS_ERROR";
export const TOGGLE_INVITE_USER_DIALOG = "TOGGLE_INVITE_USER_DIALOG";
export const INVITE_USER_ERROR = "INVITE_USER_ERROR";

const USER_API_PREFIX = RESOURCE_API_PATH + "/users/";

function fetchProjectMembersSuccessAction(members, allUsers) {
    return {
        type: FETCH_PROJECT_MEMBERS_SUCCESS,
        members: JSON.parse(members)._embedded['user-projects'],
        allUsers: JSON.parse(allUsers)._embedded.users
    }
}

function fetchProjectMembersErrorAction() {
    return {
        type: FETCH_PROJECT_MEMBERS_ERROR,
        error: "Error during fetching project info."
    }
}


export function fetchProjectMembersAction(href) {
    return (dispatch) => {
        dispatch({type: FETCH_PROJECT_MEMBERS});
        let membersPromise = Rest({
            path: href
        });
        let allUsersPromise = Rest({
            path: USER_API_PREFIX
        });
        return Promise.all([membersPromise, allUsersPromise]).then((responses) => {
            if (responses.every((response) => response.status.code === 200)) {
                    dispatch(fetchProjectMembersSuccessAction(responses[0].entity, responses[1].entity));
                } else {
                    dispatch(fetchProjectMembersErrorAction());
                }
            },
            (error) => {
                dispatch(fetchProjectMembersErrorAction());
            }
        );
    }
}

function fetchProjectMembersErrorAction() {
    return {
        type: INVITE_USER_ERROR,
        error: "Error during inviting user."
    }
}

export function inviteUserAction(data, membersHref) {
    return (dispatch) => {
        return Rest.wrap(csrf, {token: CookiesService.getCookie(CookiesService.XSRF_TOKEN), name: "X-XSRF-TOKEN"})({
            method: "POST",
            path: RESOURCE_API_PATH + "/user-projects",
            entity: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
                if(response.status.code === 201) {
                    dispatch(fetchProjectMembersAction(membersHref));
                    dispatch(toggleInviteUserDialogAction(false));
                } else {
                    dispatch(fetchProjectMembersErrorAction());
                }
            },
            (error) => {
                dispatch(fetchProjectMembersErrorAction());
            }
        );
    }
}

export function toggleInviteUserDialogAction(isOpen) {
    return {
        type: TOGGLE_INVITE_USER_DIALOG,
        isOpen
    }
}

