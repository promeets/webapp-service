import {FETCH_PROJECT_MEMBERS,
        FETCH_PROJECT_MEMBERS_ERROR,
        FETCH_PROJECT_MEMBERS_SUCCESS,
        TOGGLE_INVITE_USER_DIALOG} from "./projectMembersActionCreators"


function projectMembersReducer(state, action) {
    switch (action.type) {
        case FETCH_PROJECT_MEMBERS_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                members: action.members,
                allUsers: action.allUsers
            });
        case FETCH_PROJECT_MEMBERS_ERROR:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error
            });
        case FETCH_PROJECT_MEMBERS:
            return Object.assign({}, state, {
                isFetching: true
            });
        case TOGGLE_INVITE_USER_DIALOG: {
            return Object.assign({}, state, {
                isDialogOpen: action.isOpen
            });
        }
        default:
            return state
    }
}

export default projectMembersReducer
