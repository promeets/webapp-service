import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {Tabs, Tab} from 'material-ui/Tabs';
import ProjectInfo from './projectInfo/ProjectInfo';
import TaskList from './taskList/TaskList';
import ProjectMembers from './members/ProjectMembers'
import Loading from "../../../common/Loading";

class Project extends React.Component {

    componentWillMount() {
        this.props.fetchProjectInfo(this.props.params.projectId);
    }

    render() {
        return (
            <div style={{height: "100%"}}>
                { this.props.project.projectInfo.isFetching && <Loading/>}
                { !this.props.project.projectInfo.isFetching &&
                <Tabs
                    initialSelectedIndex={0}
                    style={{height: "100%"}}
                    tabTemplateStyle={{height: "100%"}}
                    contentContainerStyle={{height: "100%"}}
                >
                    <Tab
                        label="Project Info"
                    >
                        <ProjectInfo
                            projectInfo={this.props.project.projectInfo}
                            onEditButtonClick={this.props.toggleProjectInfoEditMode}
                            onSaveButtonClick={this.props.saveProjectInfo}
                        />
                    </Tab>

                    <Tab
                        label="Tasks"
                        style={{height: "100%"}}

                    >
                        <TaskList
                            fetchProjectTasksList={() =>
                                this.props.fetchProjectTasksList(
                                    this.props.project.projectInfo.projectEntity._links.tasks.href,
                                    this.props.project.projectInfo.projectEntity._links.users.href)}
                            projectTasksList={this.props.project.projectTaskList}
                            projectHref={this.props.project.projectInfo.projectEntity._links.self.href}
                            onTaskClick={(id) => this.props.onTaskClick(id)}
                            toggleCreateTaskDialog={this.props.toggleCreateTaskDialog}
                            createTask={(task) =>
                                this.props.createTask(task,
                                    this.props.project.projectInfo.projectEntity._links.tasks.href,
                                    this.props.project.projectInfo.projectEntity._links.users.href)}
                            style={{height: "100%"}}
                        />
                    </Tab>
                    <Tab
                        label="Members"
                        onActive={() =>
                            this.props.fetchProjectMembers(this.props.project.projectInfo.projectEntity._links.users.href)}
                    >
                        { this.props.project.projectMembers.isFetching && <Loading/>}
                        { !this.props.project.projectMembers.isFetching &&
                        <ProjectMembers
                            projectMembers={this.props.project.projectMembers}
                            toggleInviteUserDialog={this.props.toggleInviteUserDialog}
                            inviteUser={(data) =>
                                this.props.inviteUser(data, this.props.project.projectInfo.projectEntity._links.users.href)}
                            projectHref={this.props.project.projectInfo.projectEntity._links.self.href}
                        />
                        }
                    </Tab>
                </Tabs>
                }
            </div>
        )
    }
}

export default muiThemeable()(Project)