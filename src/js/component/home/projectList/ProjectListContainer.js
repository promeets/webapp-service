import React from 'react';
import {connect} from 'react-redux'
import ProjectList from './ProjectList'
import {createFetchProjectListAction,
        createToggleCreateProjectDialogAction,
        createProjectAction} from './projectListActionCreators'
import { browserHistory } from 'react-router'

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProjectList: (user) => {
            dispatch(createFetchProjectListAction(user))
        },
        onProjectListItemClick: (id) => {
            dispatch({type: "ROUTING" , to: browserHistory.push("/projects/" + id)});
        },
        toggleCreateProjectDialog: (isOpen) => {
            dispatch(createToggleCreateProjectDialogAction(isOpen))
        },
        createProject: (project, user) => {
            dispatch(createProjectAction(project, user))
        }
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        projects: state.projects
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectList)
