import {PROJECT_LIST_REQUEST,
        PROJECT_LIST_SUCCESS,
        PROJECT_LIST_ERROR,
        TOGGLE_CREATE_PROJECT_DIALOG
        } from "./projectListActionCreators"

const INITIAL_STATE = {isFetching: false, isDialogOpen: false, projectList: []};

function projectListReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case PROJECT_LIST_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                projectList: action.projectList
            });
        case PROJECT_LIST_ERROR:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error
            });
        case PROJECT_LIST_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case TOGGLE_CREATE_PROJECT_DIALOG: {
            return Object.assign({}, state, {
                isDialogOpen: action.isOpen
            });
        }
        default:
            return state
    }
}

export default projectListReducer
