import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import Formsy from 'formsy-react';
import {FormsyText} from 'formsy-material-ui/lib';
import {Card, CardHeader} from 'material-ui/Card';
import {Link} from 'react-router'
import muiThemeable from 'material-ui/styles/muiThemeable';
import LoginStyle from './login.scss';

let fontSize = 14;
const styles = {
    card: {
        width: 360,
        height: 'auto',
        margin: 'auto'
    },
    form: {
        paddingLeft: '20px',
        paddingRight: '20px'
    },
    textField: {
        fontSize: fontSize,
        width: '100%'
    },
    loginButton: {
        fontSize: fontSize,
        width: '100%',
        marginTop: '20px'
    },
    hintIcon: {
        margin: 4,
        fontSize: fontSize,
        height: 18,
        verticalAlign: 'middle'
    },
    progress: {
        display: 'block',
        margin: 'auto',
        paddingTop: '20px'
    }
}

class LoginForm extends React.Component {

    render() {
        return (
            <div>
                <div id="promeets-label">Promeets</div>
                <Card style={styles.card}>
                    <CardHeader
                        title="Login to your account"
                        style={{background: this.props.muiTheme.palette.primary1Color}}
                        titleColor="#FFFFFF"
                        titleStyle={{fontSize: "16px", marginLeft: '8px'}}
                    >
                    </CardHeader>
                    <Formsy.Form
                        onValidSubmit={this.props.onLoginClick}
                        style={styles.form}
                    >
                        <div id="error-text">{this.props.user.error ? this.props.user.error : ""}</div>
                        <FormsyText
                            name="email"
                            ref="email"
                            required
                            floatingLabelText={<span><i className="material-icons" style={styles.hintIcon}>email</i>Email</span>}
                            style={styles.textField}
                        />
                        <FormsyText
                            name="password"
                            ref="password"
                            floatingLabelText={<span><i className="material-icons" style={styles.hintIcon}>lock</i>Password</span>}
                            required
                            style={styles.textField}
                            type="password"
                        />
                        <RaisedButton
                            style={styles.loginButton}
                            type="submit"
                            label="LOG IN"
                            disabled={this.props.user.isFetching}
                            primary={true}
                        />
                    </Formsy.Form>
                    <Link to={'/sign_up'} style={{
                        display: 'inline-block', margin: '20px 0px 20px 20px',
                        color: this.props.muiTheme.palette.primary1Color, textDecoration: 'none'
                    }}>
                        <div>Sign up</div>
                    </Link>
                    <Link to={'/reset_password'} style={{
                        display: 'inline-block', float: 'right', margin: '20px 20px 20px 0px',
                        color: this.props.muiTheme.palette.primary1Color, textDecoration: 'none'
                    }}>
                        <div>Reset password</div>
                    </Link>
                </Card>
                {this.props.user.isFetching && <CircularProgress style={styles.progress}/>}
            </div>
        )
    }
}

export default muiThemeable()(LoginForm);
