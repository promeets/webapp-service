import React from 'react';
import {connect} from 'react-redux'
import LoginForm from './LoginForm'


import {createLoginAction} from './loginActionCreators'

const mapDispatchToProps = (dispatch) => {
  return {
    onLoginClick: (data) => {
      dispatch(createLoginAction(data))
    }
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)
