//import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import userReducer from './modules/common/auth/userReducer'
import headerReducer from './modules/home/header/headerReducer'
import chatReducer from './modules/home/chat/chatReducer';

// const rootReducer = combineReducers({
//   login: handleLogin,
//   routing: routerReducer
// })

//root reducer was rewritten in this way in order to easily understand its work
//in future we can pick variant above
const rootReducer = function (state = {}, action) {
    return {
        user: userReducer(state.user, action),
        routing: routerReducer(state.routing, action),
        header: headerReducer(state.header, action),
        chat: chatReducer(state.chat, action)
    }
};

export default rootReducer
