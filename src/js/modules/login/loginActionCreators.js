import Rest from 'rest';
import { browserHistory } from 'react-router'

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";

function createLoginSuccessAction(json) {
    const entity = JSON.parse(json.entity);
    return {
        type: LOGIN_SUCCESS,
        username: entity.principal.username,
        authenticated: entity.authenticated,
        authorities: entity.authorities
    }
}

function createLoginErrorAction(json) {
    return {
        type: LOGIN_ERROR,
        error: translateErrorCodeToMessage(json.status.code)
    }
}

function translateErrorCodeToMessage(code) {
    if(code == 401) {
        return "Incorrect email/password.";
    } else if (code >= 500) {
        return "Something wrong with server. Try later."
    } else {
        return "Unknown error."
    }
}

export function createLoginAction(data) {
    return (dispatch) => {
        dispatch({ type: LOGIN_REQUEST })
        return Rest({
            path: '/api/login',
            headers: {
                authorization: 'Basic ' + btoa(data.email + ':' + data.password)
            }
        }).then((response) => {
                // handle response.status.code here
                if(response.status.code === 200) {
                    dispatch(createLoginSuccessAction(response));
                    dispatch({type: "ROUTING" , to: browserHistory.push('/')}); // check this
                } else {
                    dispatch(createLoginErrorAction(response))
                }
            },
            (error) => {
                dispatch(createLoginErrorAction(error))
            }
        );
    }
}
