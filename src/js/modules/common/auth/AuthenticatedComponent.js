import React from 'react';
import {connect} from 'react-redux';
import {createAuthAction} from './authActionCreators'
import CircularProgress from 'material-ui/CircularProgress';
const PRODUCTION = process.env.NODE_ENV === 'production';
export function requireAuthentication(Component) {
    const styles = {
        progress: {
            display: 'block',
            margin: 'auto'
        }
    }

    class AuthenticatedComponent extends React.Component {
        constructor(props) {
            super(props);
        }

        componentWillMount() {
            this.checkAuth();
        }

        checkAuth() {
            if (!this.props.user.authenticated && PRODUCTION) {
                this.props.dispatch(createAuthAction());
            }
        }

        render() {
            let innerContent = null;
            if(this.props.user.authenticated || !PRODUCTION) {
                innerContent = <Component {...this.props}/>;
            } else if (!this.props.user.authenticated && this.props.user.isFetching) {
                innerContent = <CircularProgress style={styles.progress}/>;
            }
            return (
                <div>
                    {innerContent}
                </div>
            );
        }
    }

    const mapStateToProps = (state) => ({
        user: state.user
    });

    return connect(mapStateToProps)(AuthenticatedComponent);

}