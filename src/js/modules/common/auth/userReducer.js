import {AUTH_REQUEST, AUTH_SUCCESS, AUTH_ERROR} from "./authActionCreators"
import {LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_ERROR} from "../../login/loginActionCreators"
import {LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_ERROR} from "../../home/header/headerActionCreators"

function userReducer(state = {isFetching: false, authenticated: false}, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                username: action.username,
                authenticated: action.authenticated,
                authorities: action.authorities
            });
        case AUTH_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                email: action.email,
                authenticated: action.authenticated
            });
        case LOGIN_ERROR:
        case AUTH_ERROR:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error
            });
        case LOGIN_REQUEST:
        case AUTH_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case LOGOUT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                authenticated: action.authenticated
            });
        default:
            return state
    }
}

export default userReducer
