import Rest from 'rest';
import { browserHistory } from 'react-router'

export const AUTH_REQUEST = "AUTH_REQUEST";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_ERROR = "AUTH_ERROR";

function createAuthSuccessAction(json) {
    const entity = JSON.parse(json.entity);
    return {
        type: AUTH_SUCCESS,
        email: entity.email,
        authenticated: true
    }
}

function createAuthErrorAction() {
    return {
        type: AUTH_ERROR,
        error: "You need to log in."
    }
}

export function createAuthAction() {
    return (dispatch) => {
        dispatch({ type: 'AUTH_REQUEST' })
        return Rest({
            path: '/api/users/search/me'
        }).then((response) => {
                if(response.status.code === 200) {
                    dispatch(createAuthSuccessAction(response));
                 } else {
                    dispatch(createAuthErrorAction());
                    dispatch({type: "ROUTING" , to: browserHistory.push("/login")});
                }
            },
            (error) => {
                dispatch(createAuthErrorAction());
                dispatch({type: "ROUTING" , to: browserHistory.push("/login")});
            }
        );
    }
}
