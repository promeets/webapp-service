import React from 'react';
import {connect} from 'react-redux'
import Header from './Header'
import {createToggleDrawerAction, createToggleChatAction, createLogOutAction} from './headerActionCreators'

const mapDispatchToProps = (dispatch) => {
    return {
        onMenuButtonClick: () => {
            dispatch(createToggleDrawerAction())
        },
        onChatButtonClick: () => {
            dispatch(createToggleChatAction())
        },
        onLogOutClick: () => {
            dispatch(createLogOutAction())
        }
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        header: state.header
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
