import Rest from 'rest';
import { browserHistory } from 'react-router'

export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_ERROR = "LOGOUT_ERROR";
export const TOGGLE_DRAWER = "TOGGLE_DRAWER";
export const TOGGLE_CHAT = "TOGGLE_CHAT";

export function createToggleDrawerAction() {
    return {
        type: TOGGLE_DRAWER
    }
}

export function createToggleChatAction() {
    return {
        type: TOGGLE_CHAT
    }
}

function createLogOutSuccessAction() {
    return {
        type: LOGOUT_SUCCESS,
        authenticated: false
    }
}

function createLogOutErrorAction() {
    return {
        type: LOGOUT_ERROR,
        error: "You need to log in."
    }
}

export function createLogOutAction() {
    return (dispatch) => {
        dispatch({ type: LOGOUT_REQUEST })
        return Rest({
            path: '/api/logout',
            method: 'POST'
        }).then((response) => {
                if(response.status.code === 200) {
                    dispatch(createLogOutSuccessAction());
                    dispatch({type: "ROUTING" , to: browserHistory.push("/login")});
                } else {
                    dispatch(createLogOutErrorAction());
                }
            },
            (error) => {
                dispatch(createLogOutErrorAction());
            }
        );
    }
}
