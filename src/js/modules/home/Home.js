import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import muiThemeable from 'material-ui/styles/muiThemeable';
import HeaderContainer from './header/HeaderContainer';
import Chat from './chat/Chat'
import ChatContainer from './chat/ChatContainer'
import SplitPane from 'react-split-pane';
import ProjectList from './projectList/ProjectList'


class Home extends React.Component {
    render() {
        return (
            <SplitPane
                split="vertical"
                defaultSize={this.props.header.isChatOpen? 250 : 0}
                minSize={0}
                primary="second"
                allowResize={true}>
                <div style={{height: '100%'}}>
                    <HeaderContainer/>
                    <Drawer open={this.props.header.isDrawerOpen}>
                        <MenuItem onClick={this.props.onCompanyItemClick}>Company</MenuItem>
                        <MenuItem onClick={this.props.onDashboardItemClick}>Dashboard</MenuItem>
                        <MenuItem onClick={this.props.onCalendarItemClick}>Calendar</MenuItem>
                        <MenuItem onClick={this.props.onProjectsItemClick}>Projects</MenuItem>
                    </Drawer>
                    {this.props.children}
                </div>
                <div>
                    <ChatContainer/>
                </div>
            </SplitPane>
        )
    }
}

export default muiThemeable()(Home)
