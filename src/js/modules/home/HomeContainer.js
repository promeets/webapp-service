import React from 'react';
import {connect} from 'react-redux'
import Home from './Home'
import {createToggleDrawerAction} from './header/headerActionCreators'
import { browserHistory } from 'react-router'

const mapStateToProps = (state) => {
    return {
        user: state.user,
        header: state.header
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onCompanyItemClick: () => {
            dispatch({type: "ROUTING" , to: browserHistory.push("/company")});
            dispatch(createToggleDrawerAction())
        },
        onDashboardItemClick: () => {
            dispatch({type: "ROUTING" , to: browserHistory.push("/dashboard")});
            dispatch(createToggleDrawerAction())
        },
        onCalendarItemClick: () => {
            dispatch({type: "ROUTING" , to: browserHistory.push("/calendar")});
            dispatch(createToggleDrawerAction())
        },
        onProjectsItemClick: () => {
            dispatch({type: "ROUTING" , to: browserHistory.push("/projects")});
            dispatch(createToggleDrawerAction())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)
