import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {Card, CardHeader} from 'material-ui/Card';

class ChatHeader extends React.Component {
    render() {
        return (
            <div className="chat-header">
                Chat header
                <hr/>
            </div>
        )
    }
}

export default muiThemeable()(ChatHeader);
