import moment from 'moment';
export const NEW_MESSAGE = 'NEW_MASSAGE';

export function createNewMessage(message) {
    return {
        type: NEW_MESSAGE,
        message: message,
        time: moment()
    }
}
