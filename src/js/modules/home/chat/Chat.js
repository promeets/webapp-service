import React from 'react';
import ReactChatView from 'react-chatview';
import muiThemeable from 'material-ui/styles/muiThemeable';
import ChatHeader from './ChatHeader';
import ChatFooter from './footer/ChatFooter';
import ChatStyle from './chat.scss'; //Include styles into bundle

const styles = {
    container: {
        background: "#fff",
        height: "100%"
    }
};

class Chat extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={styles.container}>
                <ChatHeader/>
                <ReactChatView
                    className="chat"
                    flipped={true}
                    scrollLoadThreshold={50}>
                    {this.props.chat.messages.map((v, ix) => <div>{`${v}`}</div>)}
                </ReactChatView>
                <ChatFooter/>
            </div>
        );
    }
}

export default muiThemeable()(Chat);
