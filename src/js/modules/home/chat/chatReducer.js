import {NEW_MESSAGE} from './chatActionCreators';

const INITIAL_STATE = {messages: []};

export default function chatReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case NEW_MESSAGE:
            state.messages.unshift(action.message);
            return Object.assign({}, state, {
                messages: state.messages
            });
        default:
            return state;
    }
}
