import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import {Card, CardHeader} from 'material-ui/Card';
import {FormsyText}  from 'formsy-material-ui/lib';
import Formsy from 'formsy-react';
import {connect} from 'react-redux'
import {createNewMessage} from '../chatActionCreators'

const styles = {
    textField: {
        fontSize: 13,
        width: "100%"
    }
};
class ChatFooter extends React.Component {

    render() {
        return (
            <div className="chat-footer">
                <hr/>
                <Formsy.Form
                    onValidSubmit={this.props.onNewMessage}>
                    <FormsyText
                        style={styles.textField}
                        name="message"
                        ref="message"
                        fullWidth={false}
                        multiLine={false}
                        hintText="Enter your message here"
                    />
                </Formsy.Form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onNewMessage: (form) => {
            dispatch(createNewMessage(form.message))
        },
    }
};

export default connect(null, mapDispatchToProps)(muiThemeable()(ChatFooter));
