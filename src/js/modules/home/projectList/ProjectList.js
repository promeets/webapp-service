import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {Link} from 'react-router'

const styles = {
    container: {
        background: "#eeeeee",
        height: "100%",
        fontSize: '32px',
        textAlign: 'center'
    }
}

class ProjectList extends React.Component {
    render() {
        return (
            <div style={styles.container}>
                Project list: <br/>
                <Link to={'/projects/1'} style={{display: 'inline-block', margin: '20px 0px 20px 20px',
                    color: this.props.muiTheme.palette.accent1Color, textDecoration: 'none'}}>
                    <div>Project with id 1</div>
                </Link>
            </div>
        )
    }
}

export default muiThemeable()(ProjectList)