import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';

const styles = {
    container: {
        background: "#eeeeee",
        height: "100%",
        fontSize: '32px',
        textAlign: 'center'
    }
}

class ProjectList extends React.Component {
    render() {
        return (
            <div style={styles.container}>project list</div>
        )
    }
}

export default muiThemeable()(ProjectList)