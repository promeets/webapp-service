import React from 'react';
import { Route, Redirect, IndexRoute} from 'react-router';
import LoginContainer from './component/login/LoginContainer';
import PageNotFound from './component/common/PageNotFound';
import SignUpForm from './component/signUp/SignUpForm';
import ResetPasswordForm from './component/resetPassword/ResetPasswordForm';
import About from './component/about/About';
import CallContainer from './component/home/call/CallContainer'
import {requireAuthentication} from './component/common/auth/AuthenticatedComponent';
import HomeContainer from "./component/home/HomeContainer";
import ProjectListContainer from "./component/home/projectList/ProjectListContainer";
import Company from "./component/home/company/Company";
import ProjectContainer from "./component/home/projectList/project/ProjectContainer";
import TaskContainer from "./component/home/projectList/project/taskList/task/TaskContainer";
import Calendar from "./component/home/calendar/Calendar";
import Dashboard from "./component/home/dashboard/Dashboard";

export default (
    <div>
        <Route path="/" component={requireAuthentication(HomeContainer)}>
            <IndexRoute component={ProjectListContainer}/>
            <Route path="/company" component={Company}/>
            <Route path="/projects" component={ProjectListContainer}/>
            <Route path='/projects/:projectId' component={ProjectContainer}/>
            <Route path='/tasks/:taskId' component={TaskContainer}/>
            <Route path='/call/:callId' component={CallContainer}/>
            <Route path="/dashboard" component={Dashboard}/>
            <Route path="/calendar" component={Calendar}/>
        </Route>
        <Route path="/login" component={LoginContainer}/>
        <Route path="/sign_up" component={SignUpForm}/>
        <Route path="/reset_password" component={ResetPasswordForm}/>
        <Route path="/about" component={About}/>
        <Route path="/not_found" component={PageNotFound}/>
        <Redirect from="*" to="/not_found"/>
    </div>
)
