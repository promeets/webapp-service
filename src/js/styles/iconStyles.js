export const iconStyles = {
    smallIcon: {
        width: 24,
        height: 24,
    },
    mediumIcon: {
        width: 24,
        height: 24,
    },
    largeIcon: {
        width: 60,
        height: 60,
    },
    small: {
        width: 36,
        height: 36,
        padding: 2,
    },
    medium: {
        width: 36,
        height: 36,
        padding: 2,
    },
    large: {
        width: 120,
        height: 120,
        padding: 30,
    }
};