import React from 'react';
import {render} from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {routerMiddleware, syncHistoryWithStore} from 'react-router-redux'
import {Router, browserHistory} from 'react-router';
import {Provider} from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Style from '../scss/index.scss';
import rootReducer from './state/rootReducer'
import routes from './routes'
import injectTapEventPlugin from 'react-tap-event-plugin';
import 'rxjs';
import {createEpicMiddleware} from 'redux-observable';
import {rootEpic} from './state/rootEpic';
import reduxCatch from 'redux-catch';

injectTapEventPlugin();


function errorHandler(error, getState, lastAction, dispatch) {
    console.error(error);
    //console.debug('current state', getState());
    //console.debug('last action was', lastAction);
    // optionally dispatch an action due to the error using the dispatch parameter
}


const muiTheme = getMuiTheme({
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: Style.primaryColor,
        accent1Color: Style.accentColor
    }
});


const PRODUCTION = process.env.NODE_ENV === 'production';
const router = routerMiddleware(browserHistory);
const epicMiddleware = createEpicMiddleware(rootEpic);

if (!PRODUCTION) {
    console.log(process.env);
}

const store = createStore(
    rootReducer,
    undefined,
    compose(
        applyMiddleware(thunk, router, epicMiddleware, reduxCatch(errorHandler)),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);
let history = syncHistoryWithStore(browserHistory, store);

class Root extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <MuiThemeProvider style={Style} muiTheme={muiTheme}>
                    <Router history={history} routes={routes}/>
                </MuiThemeProvider>
            </Provider>
        )
    }
}

render(
    <Root />,
    document.getElementById('app')
);
