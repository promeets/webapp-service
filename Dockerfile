FROM openresty/openresty:alpine
COPY public /var/www
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/mime.types /etc/nginx/mime.types
RUN mkdir -p /var/log/nginx
ENV RESOURCE_API_URL http://localhost:8080
ENTRYPOINT ["/usr/local/openresty/nginx/sbin/nginx", "-c","/etc/nginx/nginx.conf", "-g", "daemon off;"]