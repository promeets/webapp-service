# Promeets web-app

## General

Main web-browser client application.

## Usage

### Default profile

#### Commands
* `npm install` - install all dependencies
* `npm start` - run webpack-dev-server
* `npm run build` - build min. bundle (public/)
    
#### API server
Connecting with api server is based on webpack-dev-server proxy: `/api -> http://localhost:8080`.  
Local environment requirements:  
* `resource-service` - see https://gitlab.com/promeets/services
