const WebpackMerge = require('webpack-merge');
const CommonConfig = require('./config/webpack.common.js');
const EnvConfig = require('./config/webpack.env.js');
const DevProxyConfig = require('./config/webpack.dev.proxy.js');
const DevProxyCloudConfig = require('./config/webpack.dev.proxy.cloud.js');
const ProdConfig = require('./config/webpack.prod.js');

const DevConfig = WebpackMerge([
    process.env.CLOUD ? DevProxyCloudConfig : DevProxyConfig
]);

module.exports = WebpackMerge([
    CommonConfig,
    EnvConfig,
    process.env.NODE_ENV === 'production' ? ProdConfig : DevConfig
]);