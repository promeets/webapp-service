const Webpack = require('webpack');
module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: process.env.CLOUD_URL || 'http://localhost:8761',
                ws: true
            }
        }
    }
};
