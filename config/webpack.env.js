const Webpack = require('webpack');

module.exports = {
    plugins: [
        new Webpack.DefinePlugin({
            'process.env': {
                RESOURCE_API_URL: JSON.stringify(process.env.RESOURCE_API_URL || 'http://localhost:8080'),
                CHAT_API_URL: JSON.stringify(process.env.CHAT_API_URL || 'http://localhost:8081'),
                CLOUD: JSON.stringify(process.env.CLOUD),
                CLOUD_URL: JSON.stringify(process.env.CLOUD_URL || 'http://localhost:8761'),
            }
        })
    ]
};