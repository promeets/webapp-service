const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    entry: {
        index: [
            './src/js/index.js'
        ]
    },
    output: {
        path: 'public',
        filename: 'js/app.min.js'
    },
    plugins: [
        new CleanWebpackPlugin(['public'], {
            verbose: true,
            dry: false
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new ExtractTextPlugin('css/styles.min.css', {
            allChunks: true
        })
    ],
    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.js?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015', 'es2016'],
                    plugins: ['transform-object-rest-spread', 'transform-class-properties', 'transform-es2015-spread']
                },
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css!sass')
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file-loader?name=/fonts/[name].[ext]'
            },
            {
                test: /\.(jp?g|png|gif|svg)$/,
                loaders: [
                    'file?hash=sha512&digest=hex&name=/images/[hash].[ext]',
                    'image-webpack'
                ]
            }
        ]
    },
    node: {
        fs: 'empty',
        tls: 'empty',
        net: 'empty',
    }
};
