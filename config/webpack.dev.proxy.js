const Webpack = require('webpack');
module.exports = {
    devServer: {
        proxy: {
            '/api/resource': {
                target: process.env.RESOURCE_API_URL || 'http://localhost:8080',
                pathRewrite: {'^/api/resource': ''},
                onProxyReq: (proxyReq) => proxyReq.setHeader('X-Forwarded-Prefix', '/api/resource')
            },
            '/api/auth': {
                target: process.env.RESOURCE_API_URL || 'http://localhost:8080',
                pathRewrite: {'^/api/auth': '/auth'},
                onProxyReq: (proxyReq) => proxyReq.setHeader('X-Forwarded-Prefix', '/api')
            },
            '/api/chat': {
                target: process.env.CHAT_API_URL || 'http://localhost:8081',
                pathRewrite: {'^/api/chat': ''},
                onProxyReq: (proxyReq) => proxyReq.setHeader('X-Forwarded-Prefix', '/api/chat'),
                ws: true
            },
            '/api/signaling/ws': {
                target: process.env.SIGNALING_API_URL || 'http://localhost:8082',
                pathRewrite: {'^/api/signaling': ''},
                onProxyReq: (proxyReq) => proxyReq.setHeader('X-Forwarded-Prefix', '/api/signaling/ws'),
                ws: true
            },
            '/api/signaling/rest': {
                target: process.env.SIGNALING_API_URL || 'http://localhost:8082',
                pathRewrite: {'^/api/signaling': ''},
                onProxyReq: (proxyReq) => proxyReq.setHeader('X-Forwarded-Prefix', '/api/signaling/rest'),
            }
        }
    }
};
